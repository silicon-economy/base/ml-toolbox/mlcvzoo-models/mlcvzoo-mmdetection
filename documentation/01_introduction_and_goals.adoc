== Introduction

*mlcvzoo_mmdetection* is a wrapper of the https://github.com/open-mmlab/mmdetection[mmdetection framework]. MMDetection is a project that provides many implementations of Object Detection, Instance Segmentation and Panoptic Segmentation algorithms (as well as "other" algorithms).
