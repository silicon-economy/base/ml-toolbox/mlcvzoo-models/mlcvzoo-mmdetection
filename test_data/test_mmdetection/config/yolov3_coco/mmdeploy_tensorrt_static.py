_base_ = [
    "MMDEPLOY_DIR/configs/mmdet/_base_/base_static.py",
    "MMDEPLOY_DIR/configs/_base_/backends/tensorrt.py",
]

# Images are reshaped to 608x480
onnx_config = dict(input_shape=(608, 480))

backend_config = dict(
    common_config=dict(max_workspace_size=1 << 30),
    model_inputs=[
        dict(
            input_shapes=dict(
                input=dict(
                    min_shape=[1, 3, 480, 608],
                    opt_shape=[1, 3, 480, 608],
                    max_shape=[1, 3, 480, 608],
                )
            )
        )
    ],
)
