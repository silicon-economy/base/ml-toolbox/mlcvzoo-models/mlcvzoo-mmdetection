class_mapping:
  mapping:
    - input_class_name: "person"
      output_class_name: "person"
    - input_class_name: "pedestrian"
      output_class_name: "person"
    - input_class_name: "staticperson"
      output_class_name: "person"
    - input_class_name: "Person"
      output_class_name: "person"
    - input_class_name: "truck"
      output_class_name: "truck"
    - input_class_name: "LKW"
      output_class_name: "truck"
    - input_class_name: "PKW"
      output_class_name: "car"
    - input_class_name: "car"
      output_class_name: "car"
  model_classes:
    - class_name: "person"
      class_id: 0
    - class_name: "truck"
      class_id: 1
    - class_name: "car"
      class_id: 2
  number_model_classes: &number_model_classes 3

inference_config:
  checkpoint_path: ""

  score_threshold: 0.3

unique_name: &unique_name "yolov3_coco_test"

train_config:
  # argparse config parameters:
  argparse_config:
    work_dir: "PROJECT_ROOT_DIR/test_output/yolov3_coco_test"

mm_config:
  config_dict: {
    "train_cfg": {
      "type": "EpochBasedTrainLoop",
      "max_epochs": 4,
      "val_interval": 7
    },
    "val_cfg": null,
    "test_cfg": {
      "type": "TestLoop"
    },
    "param_scheduler": [
      {
        "type": "LinearLR",
        "start_factor": 0.1,
        "by_epoch": false,
        "begin": 0,
        "end": 2000
      },
      {
        "type": "MultiStepLR",
        "by_epoch": true,
        "milestones": [
          218,
          246
        ],
        "gamma": 0.1
      }
    ],
    "optim_wrapper": {
      "type": "OptimWrapper",
      "optimizer": {
        "type": "SGD",
        "lr": 0.001,
        "momentum": 0.9,
        "weight_decay": 0.0005
      },
      "clip_grad": {
        "max_norm": 35,
        "norm_type": 2
      }
    },
    "auto_scale_lr": {
      "enable": false,
      "base_batch_size": 64
    },
    "default_scope": "mmdet",
    "default_hooks": {
      "timer": {
        "type": "IterTimerHook"
      },
      "logger": {
        "type": "LoggerHook",
        "interval": 1
      },
      "param_scheduler": {
        "type": "ParamSchedulerHook"
      },
      "checkpoint": {
        "type": "CheckpointHook",
        "interval": 1,
        "filename_tmpl": !join_string [ *unique_name, "_", "epoch_{}.pth" ],
      },
      "sampler_seed": {
        "type": "DistSamplerSeedHook"
      },
      "visualization": {
        "type": "DetVisualizationHook"
      }
    },
    "env_cfg": {
      "cudnn_benchmark": false,
      "mp_cfg": {
        "mp_start_method": "fork",
        "opencv_num_threads": 0
      },
      "dist_cfg": {
        "backend": "nccl"
      }
    },
    "vis_backends": [
      {
        "type": "LocalVisBackend"
      }
    ],
    "visualizer": {
      "type": "DetLocalVisualizer",
      "vis_backends": [
        {
          "type": "LocalVisBackend"
        }
      ],
      "name": "visualizer"
    },
    "log_processor": {
      "type": "LogProcessor",
      "window_size": 50,
      "by_epoch": true
    },
    "log_level": "INFO",
    # https://download.openmmlab.com/mmdetection/v2.0/yolo/yolov3_d53_mstrain-608_273e_coco/yolov3_d53_mstrain-608_273e_coco_20210518_115020-a2c3acb8.pth
    "load_from": "BASELINE_MODEL_DIR/object_detection/mmdetection/yolov3/yolov3_coco/yolov3_d53_mstrain-608_273e_coco_20210518_115020-a2c3acb8.pth",
    "resume": false,
    "data_preprocessor": {
      "type": "DetDataPreprocessor",
      "mean": [
        0,
        0,
        0
      ],
      "std": [
        255.0,
        255.0,
        255.0
      ],
      "bgr_to_rgb": true,
      "pad_size_divisor": 32
    },
    "model": {
      "type": "YOLOV3",
      "data_preprocessor": {
        "type": "DetDataPreprocessor",
        "mean": [
          0,
          0,
          0
        ],
        "std": [
          255.0,
          255.0,
          255.0
        ],
        "bgr_to_rgb": true,
        "pad_size_divisor": 32
      },
      "backbone": {
        "type": "Darknet",
        "depth": 53,
        "out_indices": [
          3,
          4,
          5
        ],
        # We are loading the complete checkpoint anyway
        "init_cfg": null
      },
      "neck": {
        "type": "YOLOV3Neck",
        "num_scales": 3,
        "in_channels": [
          1024,
          512,
          256
        ],
        "out_channels": [
          512,
          256,
          128
        ]
      },
      "bbox_head": {
        "type": "YOLOV3Head",
        "num_classes": *number_model_classes,
        "in_channels": [
          512,
          256,
          128
        ],
        "out_channels": [
          1024,
          512,
          256
        ],
        "anchor_generator": {
          "type": "YOLOAnchorGenerator",
          "base_sizes": [
            [
              [
                116,
                90
              ],
              [
                156,
                198
              ],
              [
                373,
                326
              ]
            ],
            [
              [
                30,
                61
              ],
              [
                62,
                45
              ],
              [
                59,
                119
              ]
            ],
            [
              [
                10,
                13
              ],
              [
                16,
                30
              ],
              [
                33,
                23
              ]
            ]
          ],
          "strides": [
            32,
            16,
            8
          ]
        },
        "bbox_coder": {
          "type": "YOLOBBoxCoder"
        },
        "featmap_strides": [
          32,
          16,
          8
        ],
        "loss_cls": {
          "type": "CrossEntropyLoss",
          "use_sigmoid": true,
          "loss_weight": 1.0,
          "reduction": "sum"
        },
        "loss_conf": {
          "type": "CrossEntropyLoss",
          "use_sigmoid": true,
          "loss_weight": 1.0,
          "reduction": "sum"
        },
        "loss_xy": {
          "type": "CrossEntropyLoss",
          "use_sigmoid": true,
          "loss_weight": 2.0,
          "reduction": "sum"
        },
        "loss_wh": {
          "type": "MSELoss",
          "loss_weight": 2.0,
          "reduction": "sum"
        }
      },
      "train_cfg": {
        "assigner": {
          "type": "GridAssigner",
          "pos_iou_thr": 0.5,
          "neg_iou_thr": 0.5,
          "min_pos_iou": 0
        }
      },
      "test_cfg": {
        "nms_pre": 1000,
        "min_bbox_size": 0,
        "score_thr": 0.05,
        "conf_thr": 0.005,
        "nms": {
          "type": "nms",
          "iou_threshold": 0.1
        },
        "max_per_img": 100
      }
    },
    "dataset_type": "CocoDataset",
    "data_root": "data/coco/",
    "backend_args": null,
    "train_pipeline": [
      {
        "type": "LoadImageFromFile",
        "backend_args": null
      },
      {
        "type": "LoadAnnotations",
        "with_bbox": true
      },
      {
        "type": "Expand",
        "mean": [
          0,
          0,
          0
        ],
        "to_rgb": true,
        "ratio_range": [
          1,
          2
        ]
      },
      {
        "type": "MinIoURandomCrop",
        "min_ious": [
          0.4,
          0.5,
          0.6,
          0.7,
          0.8,
          0.9
        ],
        "min_crop_size": 0.3
      },
      {
        "type": "RandomResize",
        "scale": [
          !!python/tuple
          [
            320,
            320
          ],
          !!python/tuple
          [
            608,
            608
          ]
        ],
        "keep_ratio": true
      },
      {
        "type": "RandomFlip",
        "prob": 0.5
      },
      {
        "type": "PhotoMetricDistortion"
      },
      {
        "type": "PackDetInputs"
      }
    ],
    "test_pipeline": [
      {
        "type": "LoadImageFromFile",
        "backend_args": null
      },
      {
        "type": "Resize",
        "scale": [
          608,
          608
        ],
        "keep_ratio": true
      },
      {
        "type": "LoadAnnotations",
        "with_bbox": true
      },
      {
        "type": "PackDetInputs",
        "meta_keys": [
          "img_id",
          "img_path",
          "ori_shape",
          "img_shape",
          "scale_factor"
        ]
      }
    ],
    "train_dataloader": {
      "batch_size": 1,
      "num_workers": 10,
      "persistent_workers": true,
      "sampler": {
        "type": "DefaultSampler",
        "shuffle": true
      },
      "batch_sampler": {
        "type": "AspectRatioBatchSampler"
      },
      "dataset": {
        "type": "MLCVZooMMDetDataset",
        "data_root": "data/coco/",
        "ann_file": "annotations/instances_train2017.json",
        "data_prefix": {
          "img": "train2017/"
        },
        "filter_cfg": {
          "filter_empty_gt": true,
          "min_size": 32
        },
        "pipeline": [
          {
            "type": "LoadImageFromFile",
            "backend_args": null
          },
          {
            "type": "LoadAnnotations",
            "with_bbox": true
          },
          {
            "type": "Expand",
            "mean": [
              0,
              0,
              0
            ],
            "to_rgb": true,
            "ratio_range": [
              1,
              2
            ]
          },
          {
            "type": "MinIoURandomCrop",
            "min_ious": [
              0.4,
              0.5,
              0.6,
              0.7,
              0.8,
              0.9
            ],
            "min_crop_size": 0.3
          },
          {
            "type": "RandomResize",
            "scale": [
              !!python/tuple
              [
                320,
                320
              ],
              !!python/tuple
              [
                608,
                608
              ]
            ],
            "keep_ratio": true
          },
          {
            "type": "RandomFlip",
            "prob": 0.5
          },
          {
            "type": "PhotoMetricDistortion"
          },
          {
            "type": "PackDetInputs"
          }
        ],
        "backend_args": null,
        "annotation_handler_config": !join_object [ "", "PROJECT_ROOT_DIR/test_data/test_mmdetection/config/yolov3_coco/yolov3_annotation_handler.yaml" ]
      }
    },
    "val_dataloader": null,
    "test_dataloader": {
      "batch_size": 1,
      "num_workers": 2,
      "persistent_workers": true,
      "drop_last": false,
      "sampler": {
        "type": "DefaultSampler",
        "shuffle": false
      },
      "dataset": {
        "type": "CocoDataset",
        "data_root": "data/coco/",
        "ann_file": "annotations/instances_val2017.json",
        "data_prefix": {
          "img": "val2017/"
        },
        "test_mode": true,
        "pipeline": [
          {
            "type": "LoadImageFromFile",
            "backend_args": null
          },
          {
            "type": "Resize",
            "scale": [
              608,
              608
            ],
            "keep_ratio": true
          },
          {
            "type": "LoadAnnotations",
            "with_bbox": true
          },
          {
            "type": "PackDetInputs",
            "meta_keys": [
              "img_id",
              "img_path",
              "ori_shape",
              "img_shape",
              "scale_factor"
            ]
          }
        ],
        "backend_args": null
      }
    },
    "val_evaluator": null,
    "test_evaluator": {
      "type": "CocoMetric",
      "ann_file": "data/coco/annotations/instances_val2017.json",
      "metric": "bbox",
      "backend_args": null
    },
    "lr_config": {
      "step": [
        2,
        3
      ]
    },
    "evaluation": null
  }
