_base_ = [
    "MMDEPLOY_DIR/configs/mmdet/_base_/base_panoptic-seg_static.py",
    "MMDEPLOY_DIR/configs/_base_/backends/onnxruntime.py",
]
onnx_config = dict(
    opset_version=13,
    output_names=["cls_logits", "mask_logits"],
    input_shape=[1067, 800],
)

backend_config = dict(
    common_config=dict(max_workspace_size=1 << 30),
    model_inputs=[
        dict(
            input_shapes=dict(
                input=dict(
                    min_shape=[1, 3, 800, 1067],
                    opt_shape=[1, 3, 800, 1067],
                    max_shape=[1, 3, 800, 1067],
                )
            )
        )
    ],
)
