_base_ = "MMDETECTION_DIR/configs/htc/htc_x101-64x4d-dconv-c3-c5_fpn_ms-400-1400-16xb1-20e_coco.py"
model = dict(
    roi_head=dict(
        semantic_roi_extractor=None,
        semantic_head=None,
        mask_roi_extractor=None,
        mask_head=None,
    ),
)
