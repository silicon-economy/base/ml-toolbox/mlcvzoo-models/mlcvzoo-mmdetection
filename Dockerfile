FROM nexus.apps.sele.iml.fraunhofer.de/sele/ml-toolbox/cicd/mlcvzoo-ci-image-11-7-1:latest

# Define paths. Let all path definitions always end with a '/'!

# setup development environment:
ENV BUILD_ENV_DIR="/build-env/"
ENV PROJECT_DIR="${BUILD_ENV_DIR}MLCVZoo/mlcvzoo-mmdetection/"
# External Projects:
ENV EXTERNAL_DIR="${BUILD_ENV_DIR}external/"

RUN mkdir -p "$PROJECT_DIR" "$EXTERNAL_DIR" "${PROJECT_DIR}requirements_locked"

# Disable CUDA detection to build with CUDA without a GPU
ENV FORCE_CUDA="1"

WORKDIR "$PROJECT_DIR"

COPY requirements_locked ./requirements_locked
COPY build.sh .

# Define arguments for being able to use the ml-toolbox token for the installion
# of git+https branch dependencies. Comment in when needed
#ARG ML_TOOLBOX_TOKEN_NAME
#ARG ML_TOOLBOX_TOKEN_PW

ENV VIRTUAL_ENV="${BUILD_ENV_DIR}venv"
ENV PATH="${VIRTUAL_ENV}/bin:$PATH"
ENV REQUIREMENTS_FILE="${PROJECT_DIR}requirements_locked/requirements-lock-uv-py310-all.txt"
RUN python3 -m venv "$VIRTUAL_ENV" \
    && python3 -m pip install --upgrade pip
RUN ./build.sh

# ====================================================================
# Since mmdet v2.15.0 the configuration files are stored in the following directory of the virtual environment.
# It is needed to build the correct absolute paths of configuration files during runtime.
ENV MMDETECTION_DIR="$VIRTUAL_ENV/lib/python3.10/site-packages/mmdet/.mim/"

# ====================================================================
# Checkout mmdeploy
# NOTE: The mmdeploy repository is only needed for configs.
#       An dedicated installation is not neccessary and already handled by poetry.
ENV MMDEPLOY_DIR="${EXTERNAL_DIR}mmdeploy/"
RUN git clone --depth=1 https://github.com/open-mmlab/mmdeploy.git --branch "v$(pip show mmdeploy | grep Version | awk '{print $2}')" "$MMDEPLOY_DIR"

# We don't deliver unit tests so even without --no-root we would need to set the PYTHONPATH here
ENV PYTHONPATH="$PYTHONPATH:$PROJECT_DIR"

# Cleanup uv cache
RUN uv cache clean

# ====================================================================
# Label the image
LABEL org.opencontainers.image.authors="Maximilian Otten <maximilian.otten@iml.fraunhofer.de>, Christian Hoppe <christian.hoppe@iml.fraunhofer.de" \
      org.opencontainers.image.vendor="Fraunhofer IML" \
      org.opencontainers.image.title="MLCVZoo MMDetection - Main GPU-enabled gitlab-runner container" \
      org.opencontainers.image.description="Container image for GPU enabled integration testing and continuous delivery for MLCVZoo MMDetection"
