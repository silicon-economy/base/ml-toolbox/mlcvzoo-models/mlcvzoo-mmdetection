# mmcv must be <2.1 because otherwise mmdet throws an error
# mmcv is not a direct dependency of mlcvzoo-mmdetection, but is needed by mmdet,
# which is not specified in the respective dependencies. Therefore, it has to be added here.
mmcv>=2,<2.1.0
# Currently our pipelines are put together to Work with CUDA 11.7, therefore we have to pin torch <2.1.0 for now
torch==2.0.0
torchvision==0.15.1
# tensorrt>=8.6.0 has dependencies on cuda 12
tensorrt==8.5.3.1
# setuptools version 70 results in: ImportError: cannot import name 'packaging' from 'pkg_resources'
setuptools<70
# No installation candidate on pypi, and git dependencies are not allowed as direct dependency
# for pypi packages. Therefore, we have to specify it here. It is used as dependency in an
# mmmdetection dataset for panoptic segmentation annotations
panopticapi @ git+https://github.com/cocodataset/panopticapi.git
# Numpy >= 2 triggers error:
# ImportError: numpy.core.multiarray failed to import
numpy<2
