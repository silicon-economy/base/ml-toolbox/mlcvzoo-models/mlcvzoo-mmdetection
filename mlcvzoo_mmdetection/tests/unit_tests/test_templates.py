# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests for config templates."""

from pathlib import Path
from typing import Dict, List, Type

import pytest
from config_builder import BaseConfigClass, ConfigBuilder

from mlcvzoo_mmdetection.configuration import MMDetectionConfig


def test_config_templates(
    project_root: Path,
    string_replacement_map: Dict[str, str],
) -> None:
    mmdetection_path = project_root / "config" / "templates" / "models" / "mmdetection"

    template_path_dict: Dict[Type[BaseConfigClass], List[Path]] = {
        MMDetectionConfig: [
            mmdetection_path
            / "htc"
            / "htc_x101_without-mask_PROJECT-VERSION"
            / "htc_x101_without-mask_PROJECT-VERSION.yaml",
            mmdetection_path
            / "htc"
            / "htc_x101_without-mask_coco"
            / "htc_x101_without-mask_coco.yaml",
            mmdetection_path
            / "yolov3"
            / "yolov3_d53_mstrain-608_273e_PROJECT-VERSION"
            / "yolov3_d53_mstrain-608_273e_PROJECT-VERSION.yaml",
            mmdetection_path
            / "yolov3"
            / "yolov3_d53_mstrain-608_273e_coco"
            / "yolov3_d53_mstrain-608_273e_coco.yaml",
        ],
    }

    for config_class_type, template_path_list in template_path_dict.items():
        for template_path in template_path_list:
            config_builder = ConfigBuilder(
                class_type=config_class_type,
                yaml_config_path=str(template_path),
                string_replacement_map=string_replacement_map,
                no_checks=True,
            )

            assert (
                config_builder.configuration is not None
            ), f"config_class_type: {config_class_type}, template_path: {template_path}"
