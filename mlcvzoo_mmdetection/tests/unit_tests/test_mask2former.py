# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests for Mask2Former model."""

from pathlib import Path
from typing import Dict, List, Optional
from unittest.mock import MagicMock

import numpy as np
import pytest
from mlcvzoo_base.api.data.box import Box
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from mlcvzoo_base.api.structs import Runtime
from pytest import fixture, mark
from pytest_mock import MockerFixture

from mlcvzoo_mmdetection.segmentation_model import MMSegmentationModel
from mlcvzoo_mmdetection.tests.unit_tests.conftest import (
    ExpectedSegmentation,
    verify_mmdeploy_configs,
    verify_segmentations,
)


@fixture(scope="function")
def bitmap_no_contours_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_mmdetection.segmentation_model.bitmap_to_polygon",
        return_value=(np.asarray([]), False),
    )


@pytest.fixture(name="expected_segmentations")
def expected_segmentations_fixture() -> List[ExpectedSegmentation]:
    """Provide a list of expected segmentations."""

    return [
        ExpectedSegmentation(
            polygon_count=5183,
            class_identifier=ClassIdentifier(
                class_id=41,
                class_name="cup",
            ),
            score=1.0,
            ortho_box=Box(xmin=256.0, ymin=729.0, xmax=1974.0, ymax=2189.0, angle=0.0),
            box=Box(
                xmin=245.5,
                ymin=692.0,
                xmax=1926.5,
                ymax=2170.0,
                angle=4.289153099060059,
            ),
            model_class_identifier=ClassIdentifier(
                class_id=41,
                class_name="cup",
            ),
        ),
        ExpectedSegmentation(
            polygon_count=197,
            class_identifier=ClassIdentifier(
                class_id=133,
                class_name="unknown_133",
            ),
            score=1.0,
            ortho_box=Box(
                xmin=2361.0, ymin=2157.0, xmax=2441.0, ymax=2216.0, angle=0.0
            ),
            box=Box(
                xmin=2376.5,
                ymin=2137.0,
                xmax=2417.5,
                ymax=2225.0,
                angle=56.02345657348633,
            ),
            model_class_identifier=ClassIdentifier(
                class_id=133,
                class_name="unknown_133",
            ),
        ),
        ExpectedSegmentation(
            polygon_count=13047,
            class_identifier=ClassIdentifier(
                class_id=131,
                class_name="unknown_131",
            ),
            score=1.0,
            ortho_box=Box(xmin=0.0, ymin=0.0, xmax=3648.0, ymax=1847.0, angle=0.0),
            box=Box(xmin=900.0, ymin=-900.5, xmax=2746.0, ymax=2746.5, angle=90.0),
            model_class_identifier=ClassIdentifier(
                class_id=131,
                class_name="unknown_131",
            ),
        ),
        ExpectedSegmentation(
            polygon_count=9719,
            class_identifier=ClassIdentifier(
                class_id=121,
                class_name="unknown_121",
            ),
            score=1.0,
            ortho_box=Box(xmin=0.0, ymin=1755.0, xmax=3648.0, ymax=2736.0, angle=0.0),
            box=Box(xmin=1333.0, ymin=421.5, xmax=2313.0, ymax=4068.5, angle=90.0),
            model_class_identifier=ClassIdentifier(
                class_id=121,
                class_name="unknown_121",
            ),
        ),
    ]


def test_inference(
    mmdetection_config_path: Path,
    object_detection_test_image_path: Path,
    string_replacement_map: Dict[str, str],
    expected_segmentations: List[ExpectedSegmentation],
) -> None:
    """Test predictions are correct on a test image with."""

    config_path = (
        mmdetection_config_path / "mask2former" / "mask2former_coco_test_config.yaml"
    )
    model = MMSegmentationModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
    )

    _, predictions_single = model.predict(
        data_item=str(object_detection_test_image_path)
    )
    predictions_many = model.predict_many(
        data_items=[
            str(object_detection_test_image_path),
            str(object_detection_test_image_path),
        ]
    )

    for predictions in (
        predictions_single,
        predictions_many[0][1],
        predictions_many[1][1],
    ):
        verify_segmentations(actual=predictions, expected=expected_segmentations)


@mark.usefixtures("bitmap_no_contours_mock")
def test_inference_no_contours(
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
    object_detection_test_image_path: Path,
) -> None:
    """Test predictions are correct on a test image with no contours."""

    config_path = (
        mmdetection_config_path / "mask2former" / "mask2former_coco_test_config.yaml"
    )
    model = MMSegmentationModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
    )

    _, predictions_single = model.predict(
        data_item=str(object_detection_test_image_path)
    )
    predictions_many = model.predict_many(
        data_items=[
            str(object_detection_test_image_path),
            str(object_detection_test_image_path),
        ]
    )

    for predictions in (
        predictions_single,
        predictions_many[0][1],
        predictions_many[1][1],
    ):
        assert len(predictions) == 0


@pytest.mark.parametrize(
    "runtime,box_coordinate_tolerance,box_angle_tolerance,score_tolerance,skip_polygon_length",
    [
        (
            Runtime.ONNXRUNTIME,
            0,
            None,
            None,
            False,
        ),
        (
            Runtime.ONNXRUNTIME_FLOAT16,
            3,
            4.0,
            None,
            True,
        ),
        pytest.param(
            Runtime.TENSORRT,
            0,
            None,
            None,
            False,
            marks=pytest.mark.xfail(
                reason="Mask2Former model cannot be converted to runtime TENSORRT."
            ),
        ),
    ],
    ids=["ONNXRUNTIME", "ONNXRUNTIME_FLOAT16", "TENSORRT"],
)
@pytest.mark.skip("Test is consuming to much resources for the runner")
def test_mmdeploy_inference(
    project_root: Path,
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
    object_detection_test_image_path: Path,
    runtime: str,
    box_coordinate_tolerance: int,
    box_angle_tolerance: Optional[float],
    score_tolerance: Optional[float],
    skip_polygon_length: bool,
    expected_segmentations: List[ExpectedSegmentation],
) -> None:
    """Test inference for MMDeploy models."""

    output_base_path = (
        project_root
        / "test_output"
        / "mask2former_coco_test"
        / "mmdeploy"
        / "static"
        / f"{runtime.lower()}"
    )

    config_path = (
        mmdetection_config_path
        / "mask2former"
        / "mask2former_coco_test_config_mmdeploy_static_config_dict.yaml"
    )
    model = MMSegmentationModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
        runtime=runtime,
    )

    _, predictions_single = model.predict(str(object_detection_test_image_path))
    predictions_many = model.predict_many(
        data_items=[
            str(object_detection_test_image_path),
            str(object_detection_test_image_path),
        ]
    )

    assert Path(
        getattr(
            model.configuration, f"mmdeploy_{runtime.lower()}_config"
        ).checkpoint_path
    ).exists()
    for dump_info_file in ["deploy.json", "detail.json", "pipeline.json"]:
        assert (output_base_path / dump_info_file).exists()

    for predictions in (
        predictions_single,
        predictions_many[0][1],
        predictions_many[1][1],
    ):
        verify_segmentations(
            actual=predictions,
            expected=expected_segmentations,
            box_coordinate_tolerance=box_coordinate_tolerance,
            box_angle_tolerance=box_angle_tolerance,
            score_tolerance=score_tolerance,
            skip_polygon_length=skip_polygon_length,
        )


@pytest.mark.parametrize(
    "runtime",
    [
        Runtime.ONNXRUNTIME,
        Runtime.ONNXRUNTIME_FLOAT16,
        Runtime.TENSORRT,
    ],
)
@pytest.mark.skip("Test is consuming to much resources for the runner")
def test_mmdeploy_configs(
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
    runtime,
) -> None:
    """Test that the MMDeploy Python and the dict configs are equal."""

    py_config_path = (
        mmdetection_config_path
        / "mask2former"
        / "mask2former_coco_test_config_mmdeploy_static_config_path.yaml"
    )
    dict_config_path = (
        mmdetection_config_path
        / "mask2former"
        / "mask2former_coco_test_config_mmdeploy_static_config_dict.yaml"
    )

    verify_mmdeploy_configs(
        py_config_path=py_config_path,
        dict_config_path=dict_config_path,
        model_type=MMSegmentationModel,
        string_replacement_map=string_replacement_map,
        runtime=runtime,
    )
