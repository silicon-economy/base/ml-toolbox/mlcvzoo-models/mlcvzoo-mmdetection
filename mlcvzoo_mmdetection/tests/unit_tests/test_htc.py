# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests for HTC model."""

import os
from pathlib import Path
from typing import Dict, List

import pytest
from mlcvzoo_base.api.data.bounding_box import BoundingBox
from mlcvzoo_base.api.data.box import Box
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier

from mlcvzoo_mmdetection.object_detection_model import MMObjectDetectionModel
from mlcvzoo_mmdetection.segmentation_model import MMSegmentationModel
from mlcvzoo_mmdetection.tests.unit_tests.conftest import (
    ExpectedSegmentation,
    verify_bounding_boxes,
    verify_segmentations,
)


@pytest.fixture(name="expected_bounding_boxes")
def expected_bounding_boxes_fixture() -> List[BoundingBox]:
    """Provide a list of expected bounding boxes."""

    return [
        BoundingBox(
            box=Box(xmin=242, ymin=719, xmax=1980, ymax=2199),
            class_identifier=ClassIdentifier(
                class_id=41,
                class_name="cup",
            ),
            score=0.9980654120445251,
        ),
        BoundingBox(
            box=Box(xmin=0, ymin=1686, xmax=3634, ymax=2707),
            class_identifier=ClassIdentifier(
                class_id=60,
                class_name="diningtable",
            ),
            score=0.8231877088546753,
        ),
        BoundingBox(
            box=Box(xmin=1999, ymin=1719, xmax=3356, ymax=2373),
            class_identifier=ClassIdentifier(
                class_id=43,
                class_name="knife",
            ),
            score=0.705761730670929,
        ),
    ]


@pytest.fixture(name="expected_segmentations")
def expected_segmentations_fixture() -> List[ExpectedSegmentation]:
    """Provide a list of expected segmentations."""

    return [
        ExpectedSegmentation(
            polygon_count=5112,
            class_identifier=ClassIdentifier(
                class_id=41,
                class_name="cup",
            ),
            score=0.9985542893409729,
            ortho_box=Box(
                xmin=250.0,
                ymin=720.0,
                xmax=1982.0,
                ymax=2188.0,
                angle=0.0,
            ),
            box=Box(
                xmin=250.0,
                ymin=720.0,
                xmax=1980.0,
                ymax=2186.0,
                angle=0.0,
            ),
            model_class_identifier=ClassIdentifier(
                class_id=41,
                class_name="cup",
            ),
        ),
        ExpectedSegmentation(
            polygon_count=2775,
            class_identifier=ClassIdentifier(
                class_id=43,
                class_name="knife",
            ),
            score=0.7850170135498047,
            ortho_box=Box(
                xmin=2019.0,
                ymin=1736.0,
                xmax=3341.0,
                ymax=2364.0,
                angle=0.0,
            ),
            box=Box(
                xmin=2591.0,
                ymin=1363.5,
                xmax=2797.0,
                ymax=2804.5,
                angle=66.93985748291016,
            ),
            model_class_identifier=ClassIdentifier(
                class_id=43,
                class_name="knife",
            ),
        ),
        ExpectedSegmentation(
            polygon_count=8995,
            class_identifier=ClassIdentifier(
                class_id=60,
                class_name="diningtable",
            ),
            score=0.6951285600662231,
            ortho_box=Box(
                xmin=11.0,
                ymin=1739.0,
                xmax=3630.0,
                ymax=2696.0,
                angle=0.0,
            ),
            box=Box(
                xmin=1342.0,
                ymin=408.0,
                xmax=2298.0,
                ymax=4026.0,
                angle=90.0,
            ),
            model_class_identifier=ClassIdentifier(
                class_id=60,
                class_name="diningtable",
            ),
        ),
    ]


def test_object_detection_inference(
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
    object_detection_test_image_path: Path,
    expected_bounding_boxes: List[BoundingBox],
) -> None:
    """Test predictions are correct on a test image for the object detection model."""

    config_path = (
        mmdetection_config_path / "htc_coco" / "htc_coco_without-mask_test_config.yaml"
    )
    model = MMObjectDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
    )

    _, predictions_single = model.predict(
        data_item=str(object_detection_test_image_path)
    )
    predictions_many = model.predict_many(
        data_items=[
            str(object_detection_test_image_path),
            str(object_detection_test_image_path),
        ]
    )

    for predictions in (
        predictions_single,
        predictions_many[0][1],
        predictions_many[1][1],
    ):
        verify_bounding_boxes(
            actual=predictions,
            expected=expected_bounding_boxes,
        )


def test_training(
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
) -> None:
    """Test training and checkpoint reduction."""

    config_path = (
        mmdetection_config_path / "htc_coco" / "htc_coco_without-mask_test_config.yaml"
    )
    model = MMObjectDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=False,
    )

    try:
        model.train()
    except RuntimeError as re:
        if "CUDA out of memory" in str(re):
            pytest.skip("Could not test training of htc. GPU memory is too small")
        else:
            raise re

    work_dir = Path(model.configuration.train_config.argparse_config.work_dir)

    input_checkpoint_path = work_dir / "htc_coco_test_epoch_1.pth"
    output_checkpoint_path = work_dir / "reduced_cp.pth"

    model.save_reduced_checkpoint(
        input_checkpoint_path=str(input_checkpoint_path),
        output_checkpoint_path=str(output_checkpoint_path),
    )

    assert os.path.isfile(input_checkpoint_path)
    assert os.path.isfile(output_checkpoint_path)


def test_instance_segmentation_inference(
    mmdetection_config_path: Path,
    object_detection_test_image_path: Path,
    string_replacement_map: Dict[str, str],
    expected_segmentations: List[ExpectedSegmentation],
) -> None:
    """Test predictions are correct on a test image for segmentation model."""

    config_path = (
        mmdetection_config_path / "htc_coco_seg" / "htc_coco_seg_test_config.yaml"
    )
    model = MMSegmentationModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
    )

    _, predictions_single = model.predict(
        data_item=str(object_detection_test_image_path)
    )
    predictions_many = model.predict_many(
        data_items=[
            str(object_detection_test_image_path),
            str(object_detection_test_image_path),
        ]
    )

    for predictions in (
        predictions_single,
        predictions_many[0][1],
        predictions_many[1][1],
    ):
        verify_segmentations(
            actual=predictions,
            expected=expected_segmentations,
        )
