# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests for YOLOX model."""

import os
from pathlib import Path
from typing import Dict, List

import pytest
from mlcvzoo_base.api.data.bounding_box import BoundingBox
from mlcvzoo_base.api.data.box import Box
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier

from mlcvzoo_mmdetection.object_detection_model import MMObjectDetectionModel
from mlcvzoo_mmdetection.tests.unit_tests.conftest import verify_bounding_boxes


@pytest.fixture(name="expected_bounding_boxes")
def expected_bounding_boxes_fixture() -> List[BoundingBox]:
    """Provide a list of expected bounding boxes."""

    return [
        BoundingBox(
            box=Box(xmin=240, ymin=725, xmax=1988, ymax=2181),
            class_identifier=ClassIdentifier(
                class_id=41,
                class_name="cup",
            ),
            score=0.964964,
            difficult=False,
            occluded=False,
            content="",
        ),
    ]


def test_inference(
    mmdetection_config_path: Path,
    object_detection_test_image_path: Path,
    string_replacement_map: Dict[str, str],
    expected_bounding_boxes: List[BoundingBox],
) -> None:
    """Test predictions are correct on a test image."""

    config_path = mmdetection_config_path / "yolox_coco" / "yolox_coco_test_config.yaml"
    model = MMObjectDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
    )

    _, predictions_single = model.predict(
        data_item=str(object_detection_test_image_path)
    )
    predictions_many = model.predict_many(
        data_items=[
            str(object_detection_test_image_path),
            str(object_detection_test_image_path),
        ]
    )

    for predictions in (
        predictions_single,
        predictions_many[0][1],
        predictions_many[1][1],
    ):
        verify_bounding_boxes(
            actual=predictions,
            expected=expected_bounding_boxes,
        )


def test_training(
    project_root: Path,
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
) -> None:
    """Test training and checkpoint restore."""

    config_path = (
        mmdetection_config_path
        / "yolox_coco"
        / "yolox_custom_test_config_with_mm_config_path.yaml"
    )
    model = MMObjectDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=False,
    )

    try:
        model.train()
    except RuntimeError as re:
        if "CUDA out of memory" in str(re):
            pytest.skip(
                "Could not test training of yolox (mmdetection). GPU memory is too small"
            )
        else:
            raise re

    model = MMObjectDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
    )

    # Restoring the previously trained weights as a test.
    checkpoint_path = (
        Path(model.configuration.train_config.argparse_config.work_dir)
        / "yolox_coco_test_epoch_4.pth"
    )

    assert os.path.isfile(checkpoint_path)

    model.restore(checkpoint_path=str(checkpoint_path))
