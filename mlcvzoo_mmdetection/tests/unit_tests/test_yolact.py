# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests for YOLACT model."""

from pathlib import Path
from typing import Dict, List
from unittest.mock import MagicMock

import pytest
from mlcvzoo_base.api.data.box import Box
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from pytest import fixture, mark
from pytest_mock import MockerFixture

from mlcvzoo_mmdetection.segmentation_model import MMSegmentationModel
from mlcvzoo_mmdetection.tests.unit_tests.conftest import (
    ExpectedSegmentation,
    verify_segmentations,
)


@pytest.fixture(name="expected_segmentations")
def expected_segmentations_fixture() -> List[ExpectedSegmentation]:
    """Provide a list of expected segmentations."""

    return [
        ExpectedSegmentation(
            polygon_count=5214,
            class_identifier=ClassIdentifier(
                class_id=41,
                class_name="cup",
            ),
            score=0.9835337996482849,
            ortho_box=Box(xmin=258.0, ymin=733.0, xmax=1972.0, ymax=2182.0, angle=0.0),
            box=Box(
                xmin=248.0,
                ymin=690.5,
                xmax=1924.0,
                ymax=2165.5,
                angle=4.236394882202148,
            ),
            model_class_identifier=ClassIdentifier(
                class_id=41,
                class_name="cup",
            ),
        ),
        ExpectedSegmentation(
            polygon_count=210,
            class_identifier=ClassIdentifier(
                class_id=60,
                class_name="diningtable",
            ),
            score=0.3760637938976288,
            ortho_box=Box(
                xmin=2607.0, ymin=2719.0, xmax=2704.0, ymax=2736.0, angle=0.0
            ),
            box=Box(xmin=2647.0, ymin=2679.0, xmax=2663.0, ymax=2775.0, angle=90.0),
            model_class_identifier=ClassIdentifier(
                class_id=60,
                class_name="diningtable",
            ),
        ),
        ExpectedSegmentation(
            polygon_count=9819,
            class_identifier=ClassIdentifier(
                class_id=60,
                class_name="diningtable",
            ),
            score=0.3407917320728302,
            ortho_box=Box(xmin=213.0, ymin=873.0, xmax=2826.0, ymax=2537.0, angle=0.0),
            box=Box(xmin=687.5, ymin=398.0, xmax=2350.5, ymax=3010.0, angle=90.0),
            model_class_identifier=ClassIdentifier(
                class_id=60,
                class_name="diningtable",
            ),
        ),
    ]


@fixture(scope="function")
def bitmap_no_contours_mock(mocker: MockerFixture) -> MagicMock:
    return mocker.patch(
        "mlcvzoo_mmdetection.segmentation_model.bitmap_to_polygon",
        return_value=([], False),
    )


def test_inference(
    mmdetection_config_path: Path,
    object_detection_test_image_path: Path,
    string_replacement_map: Dict[str, str],
    expected_segmentations: List[ExpectedSegmentation],
) -> None:
    """Test predictions are correct on a test image."""

    config_path = (
        mmdetection_config_path / "yolact_coco" / "yolact_coco_test_config.yaml"
    )
    model = MMSegmentationModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
    )

    _, predictions_single = model.predict(
        data_item=str(object_detection_test_image_path)
    )
    predictions_many = model.predict_many(
        data_items=[
            str(object_detection_test_image_path),
            str(object_detection_test_image_path),
        ]
    )

    for predictions in (
        predictions_single,
        predictions_many[0][1],
        predictions_many[1][1],
    ):
        verify_segmentations(
            actual=predictions,
            expected=expected_segmentations,
        )


@mark.usefixtures("bitmap_no_contours_mock")
def test_yolact_coco_inference_no_contours(
    mmdetection_config_path: Path,
    object_detection_test_image_path: Path,
    string_replacement_map: Dict[str, str],
) -> None:
    config_path = (
        mmdetection_config_path / "yolact_coco" / "yolact_coco_test_config.yaml"
    )
    model = MMSegmentationModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
    )

    _, predictions_single = model.predict(
        data_item=str(object_detection_test_image_path)
    )
    predictions_many = model.predict_many(
        data_items=[
            str(object_detection_test_image_path),
            str(object_detection_test_image_path),
        ]
    )

    for predictions in (
        predictions_single,
        predictions_many[0][1],
        predictions_many[1][1],
    ):
        assert len(predictions) == 0


def test_training(
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
) -> None:
    """Test training and checkpoint creation."""

    config_path = (
        mmdetection_config_path / "yolact_custom" / "yolact_custom_config.yaml"
    )
    model = MMSegmentationModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=False,
    )

    model.train()

    expected_file = (
        Path(model.configuration.train_config.argparse_config.work_dir)
        / "yolact_r101_1x8_coco_custom_epoch_1.pth"
    )

    assert expected_file.exists()
