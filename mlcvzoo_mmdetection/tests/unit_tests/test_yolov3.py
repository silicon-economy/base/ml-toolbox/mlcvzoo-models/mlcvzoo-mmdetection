# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests vor YOLOv3 model."""

import os
from pathlib import Path
from typing import Dict, List, Optional

import pytest
from mlcvzoo_base.api.data.bounding_box import BoundingBox
from mlcvzoo_base.api.data.box import Box
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from mlcvzoo_base.api.structs import Runtime

from mlcvzoo_mmdetection.tests.unit_tests.conftest import (
    verify_bounding_boxes,
    verify_mmdeploy_configs,
)


@pytest.fixture(name="expected_bounding_boxes")
def expected_bounding_boxes_fixture() -> List[BoundingBox]:
    """Provide a list of expected bounding boxes."""

    return [
        BoundingBox(
            box=Box(xmin=233, ymin=688, xmax=1997, ymax=2230),
            class_identifier=ClassIdentifier(
                class_id=41,
                class_name="cup",
            ),
            score=0.9513535499572754,
        ),
        BoundingBox(
            box=Box(xmin=0, ymin=1717, xmax=3305, ymax=2702),
            class_identifier=ClassIdentifier(
                class_id=60,
                class_name="diningtable",
            ),
            score=0.6292675137519836,
        ),
    ]


def test_inference(
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
    object_detection_test_image_path: str,
    expected_bounding_boxes: List[BoundingBox],
) -> None:
    """Test predictions are correct on a test image."""
    from mlcvzoo_mmdetection.object_detection_model import MMObjectDetectionModel

    config_path = (
        mmdetection_config_path / "yolov3_coco" / "yolov3_coco_test_config.yaml"
    )
    model = MMObjectDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
    )

    _, predictions_single = model.predict(
        data_item=str(object_detection_test_image_path)
    )
    predictions_many = model.predict_many(
        data_items=[
            str(object_detection_test_image_path),
            str(object_detection_test_image_path),
        ]
    )

    for predictions in (
        predictions_single,
        predictions_many[0][1],
        predictions_many[1][1],
    ):
        verify_bounding_boxes(
            actual=predictions,
            expected=expected_bounding_boxes,
        )


@pytest.mark.parametrize(
    "unload_modules",
    [["mmdeploy", "mmcv", "mmdet", "mmengine", "mlcvzoo"]],
    indirect=True,
)
def test_training_python_config(
    project_root: Path,
    mmdetection_config_path: Path,
    object_detection_test_image_path,
    string_replacement_map: Dict[str, str],
    unload_modules,
) -> None:
    """Test training with python config."""
    from mlcvzoo_mmdetection.object_detection_model import MMObjectDetectionModel

    config_path = (
        mmdetection_config_path
        / "yolov3_coco"
        / "yolov3_custom_test_config_with_mm_config_path.yaml"
    )
    model = MMObjectDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=False,
    )

    try:
        model.train()
    except RuntimeError as re:
        if "CUDA out of memory" in str(re):
            pytest.skip(
                "Could not test training of yolov3 (mmdetection). GPU memory is to small"
            )
        else:
            raise re

    assert os.path.isfile(
        os.path.join(
            project_root,
            "test_output",
            "yolov3_coco_test",
            "yolov3_coco_test_epoch_4.pth",
        )
    )

    best_metric_info, metrics = model.determine_training_metrics()
    training_logs = model.determine_training_logs()

    assert best_metric_info is None
    assert metrics == {}

    train_metric_names = [
        "train/lr",
        "train/data_time",
        "train/grad_norm",
        "train/loss",
        "train/loss_cls",
        "train/loss_conf",
        "train/loss_xy",
        "train/loss_wh",
        "train/time",
        "train/iter",
        "train/memory",
        "train/step",
    ]

    # Depending on the configuration of max-epoch and val-interval, we
    # get not all metrics for every epoch. This is what we expect to
    # be contained using the configuration of the test model.
    expected_simplified_logs: Dict[int, List[str]] = {
        index: train_metric_names for index in range(1, 5)
    }

    produced_simplified_logs = {
        epoch: list(metrics.keys()) for epoch, metrics in training_logs.items()
    }

    assert produced_simplified_logs == expected_simplified_logs


@pytest.mark.parametrize(
    "unload_modules",
    [["mmdeploy", "mmcv", "mmdet", "mmengine", "mlcvzoo"]],
    indirect=True,
)
def test_training_dict_config(
    project_root: Path,
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
    unload_modules,
) -> None:
    """Test training with dict config."""
    from mlcvzoo_mmdetection.object_detection_model import MMObjectDetectionModel

    config_path = (
        mmdetection_config_path
        / "yolov3_coco"
        / "yolov3_custom_test_config_with_mm_config.yaml"
    )
    model = MMObjectDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=False,
    )

    try:
        model.train()
    except RuntimeError as re:
        if "CUDA out of memory" in str(re):
            pytest.skip(
                "Could not test training of yolov3 (mmdetection). GPU memory is to small"
            )
        else:
            raise re

    model = MMObjectDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
    )

    model.restore(
        checkpoint_path=str(
            project_root
            / "test_output"
            / "yolov3_coco_test"
            / "yolov3_coco_test_epoch_4.pth"
        )
    )


@pytest.mark.parametrize(
    "unload_modules",
    [["mmdeploy", "mmcv", "mmdet", "mmengine", "mlcvzoo"]],
    indirect=True,
)
def test_distributed_training(
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
    unload_modules,
) -> None:
    """Test distributed training."""
    from mlcvzoo_mmdetection.object_detection_model import MMObjectDetectionModel

    config_path = (
        mmdetection_config_path
        / "yolov3_custom_distributed"
        / "yolov3_custom_distributed_config.yaml"
    )
    model = MMObjectDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=False,
    )

    try:
        model.train()
    except RuntimeError as re:
        if "CUDA out of memory" in str(re):
            pytest.skip(
                "Could not test distributed training of yolov3 (mmdetection). GPU memory is too small."
            )
        else:
            raise re


def test_mmdeploy_conversion_no_dump_info(
    project_root: Path,
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
) -> None:
    """Test that no additional info files are written if dump_info is set to false."""
    from mlcvzoo_mmdetection.object_detection_model import MMObjectDetectionModel

    output_base_path = (
        project_root
        / "test_output"
        / "yolov3_coco_test"
        / "mmdeploy"
        / "static"
        / "onnxruntime"
    )

    config_path = (
        mmdetection_config_path
        / "yolov3_coco"
        / "yolov3_coco_test_config_mmdeploy_static_config_path_no_dump.yaml"
    )
    model = MMObjectDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
        runtime=Runtime.ONNXRUNTIME,
    )

    assert Path(
        model.configuration.mmdeploy_onnxruntime_config.checkpoint_path
    ).exists()

    for dump_info_file in ["deploy.json", "detail.json", "pipeline.json"]:
        assert not (output_base_path / dump_info_file).exists()


@pytest.mark.parametrize(
    "runtime,score_tolerance,box_coordinate_tolerance",
    [
        (
            Runtime.ONNXRUNTIME,
            None,
            0,
        ),
        (
            Runtime.ONNXRUNTIME_FLOAT16,
            2e-3,
            1,
        ),
        (
            Runtime.TENSORRT,
            7.3e-2,
            73,
        ),
    ],
    ids=["ONNXRUNTIME", "ONNXRUNTIME_FLOAT16", "TENSORRT"],
)
def test_mmdeploy_inference(
    project_root: Path,
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
    object_detection_test_image_path: Path,
    expected_bounding_boxes: List[BoundingBox],
    runtime: str,
    score_tolerance: Optional[float],
    box_coordinate_tolerance: int,
) -> None:
    """Test inference for MMDeploy models."""
    from mlcvzoo_mmdetection.object_detection_model import MMObjectDetectionModel

    output_base_path = (
        project_root
        / "test_output"
        / "yolov3_coco_test"
        / "mmdeploy"
        / "static"
        / f"{runtime.lower()}"
    )

    config_path = (
        mmdetection_config_path
        / "yolov3_coco"
        / "yolov3_coco_test_config_mmdeploy_static_config_dict.yaml"
    )
    model = MMObjectDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
        runtime=runtime,
    )

    _, predictions_single = model.predict(
        data_item=str(object_detection_test_image_path)
    )
    predictions_many = model.predict_many(
        data_items=[
            str(object_detection_test_image_path),
            str(object_detection_test_image_path),
        ]
    )

    assert Path(
        getattr(
            model.configuration, f"mmdeploy_{runtime.lower()}_config"
        ).checkpoint_path
    ).exists()

    for dump_info_file in ["deploy.json", "detail.json", "pipeline.json"]:
        assert (output_base_path / dump_info_file).exists()

    for predictions in (
        predictions_single,
        predictions_many[0][1],
        predictions_many[1][1],
    ):
        verify_bounding_boxes(
            actual=predictions,
            expected=expected_bounding_boxes,
            box_coordinate_tolerance=box_coordinate_tolerance,
            score_tolerance=score_tolerance,
        )


@pytest.mark.parametrize(
    "runtime",
    [
        Runtime.ONNXRUNTIME,
        Runtime.ONNXRUNTIME_FLOAT16,
        Runtime.TENSORRT,
    ],
)
def test_mmdeploy_configs(
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
    runtime: str,
) -> None:
    """Test that the MMDeploy Python and the dict configs are equal."""
    from mlcvzoo_mmdetection.object_detection_model import MMObjectDetectionModel

    py_config_path = (
        mmdetection_config_path
        / "yolov3_coco"
        / "yolov3_coco_test_config_mmdeploy_static_config_path.yaml"
    )
    dict_config_path = (
        mmdetection_config_path
        / "yolov3_coco"
        / "yolov3_coco_test_config_mmdeploy_static_config_dict.yaml"
    )

    verify_mmdeploy_configs(
        py_config_path=py_config_path,
        dict_config_path=dict_config_path,
        model_type=MMObjectDetectionModel,
        string_replacement_map=string_replacement_map,
        runtime=runtime,
    )
