# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Definition of common pytest fixtures and utility functions for testing.
"""

import json
import logging
import os
import shutil
from pathlib import Path
from sys import modules
from typing import Dict, List, NamedTuple, Optional, Type, Union

import numpy as np
import pytest
from config_builder.replacement_map import (
    get_current_replacement_map,
    set_replacement_map_value,
    update_replacement_map_from_os,
)
from mlcvzoo_base.api.data.bounding_box import BoundingBox
from mlcvzoo_base.api.data.box import Box
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from mlcvzoo_base.api.data.segmentation import Segmentation
from mlcvzoo_base.api.structs import Runtime
from mlcvzoo_base.configuration.replacement_config import ReplacementConfig

import mlcvzoo_mmdetection
from mlcvzoo_mmdetection.object_detection_model import MMObjectDetectionModel
from mlcvzoo_mmdetection.segmentation_model import MMSegmentationModel
from mlcvzoo_mmdetection.utils import init_mm_config

logging.getLogger("matplotlib.font_manager").disabled = True

DEFAULT_ABS_FLOAT_TOLERANCE = 1e-4


@pytest.fixture(name="project_root")
def project_root_fixture() -> Path:
    """Provide the project root path."""

    this_dir = Path(os.path.dirname(os.path.abspath(__file__))).resolve()

    setup_path = this_dir
    while setup_path.exists() and setup_path.name != mlcvzoo_mmdetection.__name__:
        if setup_path == setup_path.parent:
            raise RuntimeError("Could not find setup_path!")
        else:
            setup_path = setup_path.parent
    # One more to be above the target directory
    setup_path = setup_path.parent

    return setup_path


@pytest.fixture(name="mmdetection_config_path")
def mmdetection_config_path_fixture(project_root: Path) -> Path:
    """Provide path to the test data root directory."""

    return project_root / "test_data" / "test_mmdetection" / "config"


@pytest.fixture(name="string_replacement_map")
def string_replacement_map_fixture(project_root: Path) -> Dict[str, str]:
    """Provide string replacement map for tests."""

    set_replacement_map_value(ReplacementConfig.PROJECT_ROOT_DIR_KEY, str(project_root))
    set_replacement_map_value(
        "MMDETECTION_DIR",
        str((project_root / ".." / ".." / "mmdetection").resolve()),
    )
    set_replacement_map_value(
        "MMDEPLOY_DIR",
        str((project_root / ".." / ".." / "mmdeploy").resolve()),
    )
    set_replacement_map_value(
        "BASELINE_MODEL_DIR",
        str((project_root / ".." / ".." / "modeling_data").resolve()),
    )
    update_replacement_map_from_os()

    return get_current_replacement_map()


@pytest.fixture(name="cleanup", scope="function", autouse=True)
def cleanup_fixture(project_root: Path):
    """Removes the data_output directory after the test is run."""

    yield  # Test runs now

    shutil.rmtree(project_root / "test_output", ignore_errors=True)


@pytest.fixture(name="unload_modules", scope="function")
def unload_modules_fixture(request):
    """Unloads all given modules and its children and restores them after the test."""

    modules_to_unload = request.param

    def __clean_sys_modules() -> None:
        for sys_module in list(modules.keys()):
            if any(
                [sys_module.startswith(mm_module) for mm_module in modules_to_unload]
            ):
                del modules[sys_module]

    modules_to_reload = {}

    for sys_module in list(modules.keys()):
        if any([sys_module.startswith(mm_module) for mm_module in modules_to_unload]):
            modules_to_reload[sys_module] = modules[sys_module]

    __clean_sys_modules()

    yield  # Test runs now

    __clean_sys_modules()

    for module_key, module in modules_to_reload.items():
        modules[module_key] = module


@pytest.fixture(name="object_detection_test_image_path")
def object_detection_test_image_path_fixture(project_root: Path) -> Path:
    """Provide path to an objection detection test image."""

    image_path = (
        project_root
        / "test_data"
        / "images"
        / "test_inference_task"
        / "test_object-detection_inference_image.jpg"
    )

    return image_path


@pytest.fixture(name="text_recognition_test_image_path")
def text_recognition_test_image_path_fixture(project_root: Path) -> Path:
    """Provide path to a text recognition test image."""

    image_path = (
        project_root
        / "test_data"
        / "images"
        / "test_inference_task"
        / "test_text-recognition_inference_image.jpg"
    )

    return image_path


def max_box_coordinate_difference(box_1: Box, box_2: Box) -> float:
    """Compute the maximum difference of the coordinates of the corners of two boxes.

    Args:
        box_1 (Box): The first box.
        box_2 (Box): The second box.

    Returns:
        float: The maximum difference of the coordinates of the corners of two boxes.
    """

    return float(np.max(np.abs(np.array(box_1.to_list()) - np.array(box_2.to_list()))))


def verify_bounding_boxes(
    actual: List[BoundingBox],
    expected: List[BoundingBox],
    box_coordinate_tolerance: int = 0,
    box_angle_tolerance: Optional[float] = None,
    score_tolerance: Optional[float] = None,
) -> None:
    """Assert that the actual list of bounding boxes matches the expected.

    Args:
        actual (List[BoundingBox]): List of the predicted bounding boxes.
        expected (List[BoundingBox]): List of the expected bounding boxes.
        box_coordinate_tolerance (int, optional): Maximum tolerance for the coordinates of the
            corners of the bounding boxes. Defaults to 0.
        box_angle_tolerance (Optional[float], optional): Maximum tolerance for the box angle.
            Defaults to None.
        score_tolerance (Optional[float], optional): Maximum tolerance for the score. Defaults to
            None.
    """

    def error_message(index: int, attribute: str) -> str:
        """Generate an error message for a bounding box mismatch.

        Args:
            index (int): The index of the mismatching bounding box.
            attribute (str): The attribute of the bounding boxes, that do no match.

        Returns:
            str: The generated error message.
        """

        return f"Bounding Box {index} mismatching '{attribute}'"

    if box_angle_tolerance is None:
        box_angle_tolerance = DEFAULT_ABS_FLOAT_TOLERANCE
    if score_tolerance is None:
        score_tolerance = DEFAULT_ABS_FLOAT_TOLERANCE

    assert len(actual) == len(expected)

    for i, (actual_bounding_box, expected_bounding_box) in enumerate(
        zip(actual, expected)
    ):
        # class_id
        assert (
            actual_bounding_box.class_id == expected_bounding_box.class_id
        ), error_message(index=i, attribute="class_id")
        # class_name
        assert (
            actual_bounding_box.class_name == expected_bounding_box.class_name
        ), error_message(index=i, attribute="class_name")
        # box
        assert (
            max_box_coordinate_difference(
                actual_bounding_box.box(), expected_bounding_box.box()
            )
            <= box_coordinate_tolerance
        ), error_message(index=i, attribute="box_coordinates")
        assert actual_bounding_box.box().angle == pytest.approx(
            expected=expected_bounding_box.box().angle,
            rel=0.0,
            abs=box_angle_tolerance,
        ), error_message(index=i, attribute="box_angle")
        # score
        assert actual_bounding_box.score == pytest.approx(
            expected=expected_bounding_box.score,
            rel=0.0,
            abs=score_tolerance,
        ), error_message(index=i, attribute="score")


class ExpectedSegmentation(NamedTuple):
    """A simplified version of the Segmentation class to validate against."""

    polygon_count: int
    class_identifier: ClassIdentifier
    score: float = 0.0
    ortho_box: Optional[Box] = None
    box: Optional[Box] = None
    model_class_identifier: Optional[ClassIdentifier] = None
    content: str = ""
    occluded: bool = False


def verify_segmentations(
    actual: List[Segmentation],
    expected: List[ExpectedSegmentation],
    box_coordinate_tolerance: int = 0,
    box_angle_tolerance: Optional[float] = None,
    score_tolerance: Optional[float] = None,
    skip_polygon_length: bool = False,
):
    """Assert that the actual list of segmentations matches the expected.

    Args:
        actual (List[Segmentation]): List of predicted segmentations.
        expected (List[ExpectedSegmentation]): List of expected segmentations.
        box_coordinate_tolerance (int, optional): Maximum tolerance for the coordinates of the
            corners of the bounding boxes. Defaults to 0.
        box_angle_tolerance (Optional[float], optional): Maximum tolerance for the box angle.
            Defaults to None.
        score_tolerance (Optional[float], optional): Maximum tolerance for the score. Defaults to
            None.
        skip_polygon_length (bool, optional): If True, do not check the number of polygon points.
            Defaults to False.
    """

    def error_message(index: int, attribute: str) -> str:
        """Generate an error message for a segmentation mismatch.

        Args:
            index (int): The index of the mismatching segmentation.
            attribute (str): The attribute of the segmentations, that do no match.

        Returns:
            str: The generated error message.
        """

        return f"Segmentation {index} mismatching '{attribute}'"

    if box_angle_tolerance is None:
        box_angle_tolerance = DEFAULT_ABS_FLOAT_TOLERANCE
    if score_tolerance is None:
        score_tolerance = DEFAULT_ABS_FLOAT_TOLERANCE

    assert len(actual) == len(expected)

    for i, (actual_segmentation, expected_segmentation) in enumerate(
        zip(actual, expected)
    ):
        # class_id
        assert (
            actual_segmentation.class_id
            == expected_segmentation.class_identifier.class_id
        ), error_message(index=i, attribute="class_id")
        # class_name
        assert (
            actual_segmentation.class_name
            == expected_segmentation.class_identifier.class_name
        ), error_message(index=i, attribute="class_name")
        # content
        assert (
            actual_segmentation.content == expected_segmentation.content
        ), error_message(index=i, attribute="content")
        # occluded
        assert (
            actual_segmentation.occluded == expected_segmentation.occluded
        ), error_message(index=i, attribute="occluded")
        # score
        assert actual_segmentation.score == pytest.approx(
            expected=expected_segmentation.score,
            rel=0.0,
            abs=score_tolerance,
        ), error_message(index=i, attribute="score")
        # polygon count
        if not skip_polygon_length:
            assert (
                len(actual_segmentation.polygon())
                == expected_segmentation.polygon_count
            ), error_message(index=i, attribute="polygon_count")
        # ortho_box
        assert (
            max_box_coordinate_difference(
                actual_segmentation.ortho_box(), expected_segmentation.ortho_box
            )
            <= box_coordinate_tolerance
        ), error_message(index=i, attribute="ortho_box")
        assert actual_segmentation.ortho_box().angle == 0
        # box
        assert (
            max_box_coordinate_difference(
                actual_segmentation.box(), expected_segmentation.box
            )
            <= box_coordinate_tolerance
        ), error_message(index=i, attribute="box_coordinates")
        assert actual_segmentation.box().angle == pytest.approx(
            expected=expected_segmentation.box.angle,
            rel=0.0,
            abs=box_angle_tolerance,
        ), error_message(index=i, attribute="box_angle")
        # model_class_identifier
        assert (
            actual_segmentation.model_class_identifier.class_id
            == expected_segmentation.model_class_identifier.class_id
        ), error_message(index=i, attribute="model_class_identifier.class_id")
        assert (
            actual_segmentation.model_class_identifier.class_name
            == expected_segmentation.model_class_identifier.class_name
        ), error_message(index=i, attribute="model_class_identifier.class_name")


def verify_mmdeploy_configs(
    py_config_path: Path,
    dict_config_path: Path,
    model_type: Union[Type[MMObjectDetectionModel], Type[MMSegmentationModel]],
    string_replacement_map: Dict[str, str],
    runtime: str,
) -> None:
    """Test that the MMDeploy Python and the dict configs are equal.

    Args:
        py_config_path (Path): Path to the MMDeploy Python config.
        dict_config_path (Path): Path to the MMDeploy dict config.
        model_type (Union[Type[MMOCRTextDetectionModel], Type[MMOCRTextRecognitionModel]]): The
            model type.
        string_replacement_map (Dict[str, str]): The string replacement map.
        runtime (str): The runtime.
    """

    py_model = model_type(
        from_yaml=str(py_config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
        runtime=Runtime.DEFAULT,
    )
    dict_model = model_type(
        from_yaml=str(dict_config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
        runtime=Runtime.DEFAULT,
    )

    py_model_cfg_dict = (
        py_model.cfg._cfg_dict  # pylint: disable=protected-access,no-member
    )
    dict_model_cfg_dict = (
        dict_model.cfg._cfg_dict  # pylint: disable=protected-access,no-member
    )
    py_mmdelpoy_cfg_dict = init_mm_config(  # pylint: disable=protected-access,no-member
        mm_config=getattr(py_model.configuration, f"mmdeploy_{runtime.lower()}_config")
    )._cfg_dict
    dict_mmdelpoy_cfg_dict = (
        init_mm_config(  # pylint: disable=protected-access,no-member
            mm_config=getattr(
                dict_model.configuration, f"mmdeploy_{runtime.lower()}_config"
            )
        )._cfg_dict
    )

    assert py_model_cfg_dict == dict_model_cfg_dict

    # It doesn't matter if a sequence is a list or a tuple
    # Serializing and desirializing result in lists for both
    assert json.loads(json.dumps(py_mmdelpoy_cfg_dict)) == json.loads(
        json.dumps(dict_mmdelpoy_cfg_dict)
    )
