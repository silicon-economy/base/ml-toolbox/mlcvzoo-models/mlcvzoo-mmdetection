# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests for classes MMDetectionConfig and MLCVZooMMDetDataset."""

import copy
from pathlib import Path
from typing import Dict, cast

import pytest
from config_builder import ConfigBuilder
from mlcvzoo_base.configuration.annotation_handler_config import AnnotationHandlerConfig
from mmengine.config import ConfigDict
from related import to_model

from mlcvzoo_mmdetection.configuration import MMDetectionConfig
from mlcvzoo_mmdetection.mlcvzoo_mmdet_dataset import MLCVZooMMDetDataset
from mlcvzoo_mmdetection.object_detection_model import MMObjectDetectionModel


def test_configuration(
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
):
    """Test MMDetectionConfig."""

    config_path = (
        mmdetection_config_path / "yolov3_coco" / "yolov3_coco_test_config.yaml"
    )
    configuration = MMObjectDetectionModel.create_configuration(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
    )

    assert configuration is not None

    configuration_2 = cast(
        MMDetectionConfig, to_model(MMDetectionConfig, configuration.to_dict())
    )
    assert configuration_2 is not None


def test_mmdet_api_methods(
    mmdetection_config_path: Path,
    project_root: Path,
    string_replacement_map: Dict[str, str],
) -> None:
    """
    Test that the base_config is correctly set when using multiple models:
        - test_ensure_correct_base_config(self)
    """

    config_path = (
        mmdetection_config_path / "yolov3_coco" / "yolov3_coco_test_config.yaml"
    )
    model = MMObjectDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
    )

    string_replacement_map["MODEL_CONFIG_ROOT_DIR"] = str(
        mmdetection_config_path / "htc_coco"
    )

    assert model.get_training_output_dir() == str(
        project_root / "test_output" / "yolov3_coco_test"
    )

    assert model.unique_name == "yolov3_coco_test"
    assert model.get_checkpoint_filename_suffix() == ".pth"


def test_init_mmdet_from_config(
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
) -> None:
    """
    Test that a mmdetection model can be initialized via a given configuration object:
        - test_init_mmdet_from_config(self)
    """

    from_yaml = str(
        mmdetection_config_path / "htc_coco" / "htc_coco_without-mask_test_config.yaml"
    )

    string_replacement_map["MODEL_CONFIG_ROOT_DIR"] = str(
        mmdetection_config_path / "htc_coco"
    )

    config_builder = ConfigBuilder(
        class_type=MMDetectionConfig,
        yaml_config_path=from_yaml,
        string_replacement_map=string_replacement_map,
    )

    configuration = cast(MMDetectionConfig, copy.deepcopy(config_builder.configuration))
    del config_builder

    configuration.recursive_string_replacement()

    model = MMObjectDetectionModel(
        configuration=configuration,
        init_for_inference=True,
        string_replacement_map=string_replacement_map,
    )

    assert model is not None


@pytest.mark.parametrize(
    "config_dict, expected_with_bbox, expected_with_mask, expected_box_type",
    [
        ({"type": "mmdet.LoadAnnotations", "with_bbox": True}, True, False, "hbox"),
        ({"type": "mmdet.LoadAnnotations", "with_bbox": False}, False, False, "hbox"),
        ({"type": "mmdet.LoadAnnotations", "with_mask": True}, False, True, "hbox"),
        (
            {"type": "mmdet.LoadAnnotations", "with_bbox": True, "with_mask": True},
            True,
            True,
            "hbox",
        ),
        (
            {"type": "mmdet.LoadAnnotations", "with_bbox": False, "with_mask": True},
            False,
            True,
            "hbox",
        ),
        ({"type": "mmdet.LoadAnnotations", "box_type": "qbox"}, False, False, "qbox"),
        (
            {"type": "mmdet.LoadAnnotations", "with_bbox": True, "box_type": "qbox"},
            True,
            False,
            "qbox",
        ),
    ],
)
def test_mlcvzoo_mmdet_dataset(
    project_root, config_dict, expected_with_bbox, expected_with_mask, expected_box_type
) -> None:
    """Test MLCVZooMMDetDataset attributes."""

    string_replacement_map = dict()
    string_replacement_map["PROJECT_ROOT_DIR"] = str(project_root)

    args = {
        "pipeline": [
            config_dict,
        ],
        "annotation_handler_config": ConfigDict(
            cast(
                AnnotationHandlerConfig,
                ConfigBuilder(
                    class_type=AnnotationHandlerConfig,
                    yaml_config_path=str(
                        project_root
                        / "test_data"
                        / "test_mmdetection"
                        / "config"
                        / "annotation-handler_coco_test.yaml",
                    ),
                    string_replacement_map=string_replacement_map,
                ).configuration,
            ).to_dict()
        ),
        "class_mapping_config": None,
    }

    mlcvzoo_mmdet_dataset = MLCVZooMMDetDataset(
        **args,
    )

    assert (
        mlcvzoo_mmdet_dataset._with_bbox  # pylint: disable=protected-access
        == expected_with_bbox
    )
    assert (
        mlcvzoo_mmdet_dataset._with_mask  # pylint: disable=protected-access
        == expected_with_mask
    )
    assert (
        mlcvzoo_mmdet_dataset._box_type  # pylint: disable=protected-access
        == expected_box_type
    )
