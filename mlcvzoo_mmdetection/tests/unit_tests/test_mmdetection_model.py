# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests for class MMDetectionModel."""

from pathlib import Path
from typing import Dict
from unittest import mock

import pytest
from mlcvzoo_base.api.structs import Runtime

from mlcvzoo_mmdetection.object_detection_model import MMObjectDetectionModel


@pytest.fixture(name="model")
def model_fixture(
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
) -> MMObjectDetectionModel:
    """Create a MMObjectDetectionModel from the yolov3_coco_test_config."""

    config_path = (
        mmdetection_config_path / "yolov3_coco" / "yolov3_coco_test_config.yaml"
    )
    model = MMObjectDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
        runtime=Runtime.DEFAULT,
    )
    return model


def test_deploy_runtime_not_supported(
    model: MMObjectDetectionModel,
) -> None:
    """Test that an exception is raised, if __deploy is called with init_for_inference=True and
    an unsupported runtime."""

    model.runtime = "INVALID"

    with pytest.raises(
        ValueError, match="Deployment is not supported for runtime 'INVALID'."
    ):
        __deploy = (
            model._MMDetectionModel__deploy  # pylint: disable=protected-access,no-member
        )
        __deploy(runtime=model.runtime, init_for_inference=True)


@pytest.mark.parametrize(
    "runtime",
    [
        Runtime.ONNXRUNTIME,
        Runtime.ONNXRUNTIME_FLOAT16,
        Runtime.TENSORRT,
    ],
)
def test_deploy_no_training(
    model: MMObjectDetectionModel,
    runtime: str,
) -> None:
    """Test that an exception is raised, if __deploy is called with init_for_inference=False."""

    model.runtime = runtime

    with pytest.raises(
        ValueError,
        match=f"Deploying for training is not supported for runtime '{runtime}'.",
    ):
        __deploy = (
            model._MMDetectionModel__deploy  # pylint: disable=protected-access,no-member
        )
        __deploy(runtime=model.runtime, init_for_inference=False)


@pytest.mark.parametrize(
    "runtime",
    [
        Runtime.ONNXRUNTIME,
        Runtime.ONNXRUNTIME_FLOAT16,
        Runtime.TENSORRT,
    ],
)
def test_deploy_mmdeploy_config_missing(
    model: MMObjectDetectionModel,
    runtime: str,
) -> None:
    """Test that an exception is raised, if __deploy is called with init_for_inference=True and
    the mmdeploy_*_config is missing."""

    model.runtime = runtime

    with pytest.raises(
        ValueError,
        match=f"The mmdeploy_{runtime.lower()}_config must be provided for runtime '{runtime}'.",
    ):
        __deploy = (
            model._MMDetectionModel__deploy  # pylint: disable=protected-access,no-member
        )
        __deploy(runtime=model.runtime, init_for_inference=True)


def test_init_inference_model_runtime_not_supported(
    model: MMObjectDetectionModel,
) -> None:
    """Test that an exception is raised, if _init_inference_model is called for an unsupported
    runtime."""

    model.runtime = "INVALID"

    with pytest.raises(
        ValueError,
        match="Initialization for inference is not supported for runtime 'INVALID'.",
    ):
        model._init_inference_model()  # pylint: disable=protected-access


@pytest.mark.parametrize(
    "runtime",
    [
        Runtime.ONNXRUNTIME,
        Runtime.ONNXRUNTIME_FLOAT16,
        Runtime.TENSORRT,
    ],
)
def test_init_inference_model_config_missing(
    model: MMObjectDetectionModel,
    runtime: str,
) -> None:
    """Test that an exception is raised, if _init_inference_model is called and the
    mmdeploy_*_config is missing."""

    model.runtime = runtime

    with pytest.raises(
        ValueError,
        match=f"The mmdeploy_{runtime.lower()}_config must be provided for runtime '{runtime}'.",
    ):
        model._init_inference_model()  # pylint: disable=protected-access


def test_init_training_model_runtime_not_supported(
    model: MMObjectDetectionModel,
) -> None:
    """Test that an exception is raised, if _init_training_model is called for an unsupported
    runtime."""

    model.runtime = "INVALID"

    with pytest.raises(
        ValueError,
        match="Initialization for training is not supported for runtime 'INVALID'.",
    ):
        model._init_training_model()  # pylint: disable=protected-access


@pytest.mark.parametrize(
    "runtime",
    [
        "INVALID",
        Runtime.ONNXRUNTIME,
        Runtime.ONNXRUNTIME_FLOAT16,
        Runtime.TENSORRT,
    ],
)
def test_init_training_model(
    model: MMObjectDetectionModel,
    runtime: str,
) -> None:
    """Test that an exception is raised, if _init_training_model is called."""

    model.runtime = runtime

    with pytest.raises(
        ValueError,
        match=f"Initialization for training is not supported for runtime '{runtime}'.",
    ):
        model._init_training_model()  # pylint: disable=protected-access


@pytest.mark.parametrize(
    "runtime",
    [
        Runtime.ONNXRUNTIME,
        Runtime.ONNXRUNTIME_FLOAT16,
        Runtime.TENSORRT,
    ],
)
def test_get_mmdeploy_config_config_missing(
    model: MMObjectDetectionModel,
    runtime: str,
) -> None:
    """Test that an exception is raised, if __get_mmdeploy_config is called and
    the mmdeploy_*_config is missing."""

    model.runtime = runtime

    with pytest.raises(
        ValueError,
        match=f"The mmdeploy_{runtime.lower()}_config must be provided for runtime '{runtime}'.",
    ):
        model._get_mmdeploy_config()  # pylint: disable=protected-access,no-member


def test_get_mmdeploy_config_runtime_not_supported(
    model: MMObjectDetectionModel,
) -> None:
    """Test that an exception is raised, if __get_mmdeploy_config is called for an unsupported
    runtime."""

    model.runtime = "INVALID"

    with pytest.raises(
        ValueError,
        match="Getting MMDeploy config is not supported for runtime 'INVALID'.",
    ):
        model._get_mmdeploy_config()  # pylint: disable=protected-access,no-member


def test_get_mmdeploy_config_runtime_not_initialized(
    model: MMObjectDetectionModel,
) -> None:
    """Test that an exception is raised, if __get_mmdeploy_config is called for an unsupported
    runtime."""

    del model.runtime

    with pytest.raises(
        ValueError,
        match="The 'rumtime' attribute is not initialized, make sure to call with a runtime or "
        "after initialization.",
    ):
        model._get_mmdeploy_config()  # pylint: disable=protected-access,no-member


def test_get_mmdeploy_config_result_default(
    model: MMObjectDetectionModel,
) -> None:
    """Test that __get_mmdeploy_config returns None if runtime DEFAULT."""

    assert (
        model._get_mmdeploy_config()  # pylint: disable=protected-access,no-member
        is None
    )


@pytest.mark.parametrize(
    "runtime",
    [
        Runtime.ONNXRUNTIME,
        Runtime.ONNXRUNTIME_FLOAT16,
        Runtime.TENSORRT,
    ],
)
def test_get_mmdeploy_config_result(
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
    runtime: str,
) -> None:
    """Test that __get_mmdeploy_config returns the correct configuration."""

    config_path = (
        mmdetection_config_path
        / "yolov3_coco"
        / "yolov3_coco_test_config_mmdeploy_static_config_path.yaml"
    )
    model = MMObjectDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
        runtime=Runtime.DEFAULT,
    )

    model.runtime = runtime

    assert (
        getattr(model.configuration, f"mmdeploy_{runtime.lower()}_config")
        == model._get_mmdeploy_config()  # pylint: disable=protected-access,no-member
    )


@pytest.mark.parametrize(
    "unload_modules",
    [["mmdeploy", "mmcv", "mmdet", "mmengine", "mlcvzoo"]],
    indirect=True,
)
def test_conversion_only_runs_if_checkpoint_paths_dont_exist(
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
    unload_modules,
) -> None:
    """Test that the MMDeployConverter is only instantiated and run, if the checkpoint_paths in
    the MMDeployModelConfig do not exist."""

    # Reimport
    from mlcvzoo_mmdetection.mlcvzoo_mmdeploy.converter import MMDeployConverter

    # As the MMDeployConverter is not imported at toplevel, we can patch sys.modules
    mmdeploy_converter_mock = mock.MagicMock(
        name="mmdeploy_converter_mock", side_effect=MMDeployConverter
    )
    mmdeploy_converter_module = mock.MagicMock(name="mmdeploy_converter_mock")
    mmdeploy_converter_module.MMDeployConverter = mmdeploy_converter_mock

    with mock.patch.dict(
        "sys.modules",
        {"mlcvzoo_mmdetection.mlcvzoo_mmdeploy.converter": mmdeploy_converter_module},
    ):
        # Import MMObjectDetectionModel after patching
        from mlcvzoo_mmdetection.object_detection_model import MMObjectDetectionModel

        assert mmdeploy_converter_mock.call_count == 0

        config_path = (
            mmdetection_config_path
            / "yolov3_coco"
            / "yolov3_coco_test_config_mmdeploy_static_config_path.yaml"
        )
        model = MMObjectDetectionModel(
            from_yaml=str(config_path),
            string_replacement_map=string_replacement_map,
            init_for_inference=True,
            runtime=Runtime.ONNXRUNTIME,
        )

        assert mmdeploy_converter_mock.call_count == 1

        # Access private method
        __deploy = (
            model._MMDetectionModel__deploy  # pylint: disable=protected-access,no-member
        )
        __deploy(runtime=model.runtime, init_for_inference=True)

        assert mmdeploy_converter_mock.call_count == 1


@pytest.mark.parametrize(
    "unload_modules",
    [["mmdeploy", "mmcv", "mmdet", "mmengine", "mlcvzoo"]],
    indirect=True,
)
@pytest.mark.parametrize(
    "runtime",
    [
        Runtime.ONNXRUNTIME,
        Runtime.ONNXRUNTIME_FLOAT16,
        Runtime.TENSORRT,
    ],
)
def test_conversion_fails_if_mmdeploy_not_found(
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
    unload_modules,
    runtime: str,
) -> None:
    """Test that an exception is raised, if __deploy is called and module mmdeploy is not found."""

    with mock.patch.dict("sys.modules", {"mmdeploy": None}):
        with pytest.raises(
            RuntimeError,
            match="Extra 'mmdeploy' must be installed to deploy a model with MMDeploy.",
        ):
            from mlcvzoo_mmdetection.object_detection_model import (
                MMObjectDetectionModel,
            )

            config_path = (
                mmdetection_config_path
                / "yolov3_coco"
                / "yolov3_coco_test_config_mmdeploy_static_config_path.yaml"
            )
            MMObjectDetectionModel(
                from_yaml=str(config_path),
                string_replacement_map=string_replacement_map,
                init_for_inference=True,
                runtime=runtime,
            )


@pytest.mark.parametrize(
    "unload_modules",
    [["mmdeploy", "mmcv", "mmdet", "mmengine", "mlcvzoo"]],
    indirect=True,
)
@pytest.mark.parametrize(
    "runtime",
    [
        Runtime.ONNXRUNTIME,
        Runtime.ONNXRUNTIME_FLOAT16,
        Runtime.TENSORRT,
    ],
)
def test_init_for_inference_fails_if_mmdeploy_not_found(
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
    unload_modules,
    runtime: str,
):
    """Test that an exception is raised, if _init_for_inference is called and module mmdeploy is not
    found."""

    with mock.patch.dict("sys.modules", {"mmdeploy": None}):
        with pytest.raises(
            RuntimeError,
            match="Extra 'mmdeploy' must be installed to run a model which is deployed with "
            "MMDeploy.",
        ):
            from mlcvzoo_mmdetection.object_detection_model import (
                MMObjectDetectionModel,
            )

            config_path = (
                mmdetection_config_path
                / "yolov3_coco"
                / "yolov3_coco_test_config_mmdeploy_static_config_path.yaml"
            )
            model = MMObjectDetectionModel(
                from_yaml=str(config_path),
                string_replacement_map=string_replacement_map,
                init_for_inference=True,
                runtime=Runtime.DEFAULT,
            )

            model.runtime = runtime

            model._init_inference_model()  # pylint: disable=protected-access
