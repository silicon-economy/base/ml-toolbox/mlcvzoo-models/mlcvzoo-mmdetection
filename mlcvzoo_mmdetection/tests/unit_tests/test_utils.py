# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests for mlcvzoo-mmdetection utility function module."""

import os
from pathlib import Path
from typing import Any, Dict

import pytest
from config_builder.config_builder import ConfigBuilder
from mlcvzoo_base.api.data.types import MetricInfo
from mmengine.config import Config as MMConfigMMDetection

from mlcvzoo_mmdetection.configuration import MMConfig, MMDetectionConfig
from mlcvzoo_mmdetection.utils import (
    determine_logs_path,
    extract_best_metric,
    extract_logs,
    init_mm_config,
    init_mmdetection_config,
    modify_config,
    parse_logs,
)


@pytest.fixture(name="configs")
def configs_fixture(
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
) -> Dict[str, Any]:
    """Prepare common configurations for all tests."""

    path_mm_config_py = str(
        mmdetection_config_path / "yolov3_coco" / "yolov3_custom.py"
    )
    # the replacement values in the .py config file have to be modified according to the replacement
    # map first so that a *_local.py file can be generated with the correct paths during runtime
    path_mm_config_local_py = modify_config(
        config_path=path_mm_config_py,
        string_replacement_map=string_replacement_map,
    )

    config_path = (
        mmdetection_config_path
        / "yolov3_coco"
        / "yolov3_custom_test_config_with_mm_config.yaml"
    )
    mm_config: MMConfig = ConfigBuilder(
        class_type=MMDetectionConfig,
        yaml_config_path=str(config_path),
        string_replacement_map=string_replacement_map,
    ).configuration.mm_config

    return {
        "path_mm_config_py": path_mm_config_py,
        "path_mm_config_local_py": path_mm_config_local_py,
        "mm_config": mm_config,
    }


@pytest.fixture(name="extracted_train_logs")
def extracted_train_logs_fixture():
    """Fixture to get a dictionary of extracted training logs."""
    return {
        1: {
            "train_loss_metric": 1.0,
            "train_precision_metric": 0.1,
            "train_accuracy_metric": 0.1,
        },
        2: {
            "train_loss_metric": 2.0,
            "train_precision_metric": 0.2,
            "train_accuracy_metric": 0.2,
            "val_accuracy_metric": 0.2,
        },
        3: {
            "train_loss_metric": 3.0,
            "train_precision_metric": 0.3,
            "train_accuracy_metric": 0.3,
        },
        4: {
            "train_loss_metric": 4.0,
            "train_precision_metric": 0.4,
            "train_accuracy_metric": 0.4,
            "val_accuracy_metric": 0.4,
        },
    }


def test_extract_best_metrics(extracted_train_logs):
    """Test extract_best_metrics function."""
    best_metric, all_metrics = extract_best_metric(
        work_dir="/test/workdir",
        filename_template="test-file.jpg",
        logs=extracted_train_logs,
        best_metric_name="train_accuracy_metric",
    )
    assert best_metric.epoch == 4
    assert best_metric.score == 0.4

    expected_metrics = {
        "/test/workdir/test-file.jpg": MetricInfo(
            path="/test/workdir/test-file.jpg",
            epoch=4,
            score=0.4,
            name="train_accuracy_metric",
        ),
    }

    assert all_metrics == expected_metrics


def test_modify_config(
    string_replacement_map: Dict[str, str],
    configs: Dict[str, Any],
) -> None:
    """Test if all replacement values in a MMDetection *.py config file have been
    replaced and saved to a *_local.py file."""

    path_mm_config_py = configs["path_mm_config_py"]
    path_mm_config_local_py = configs["path_mm_config_local_py"]

    # modified config file is created in configs fixture
    assert path_mm_config_local_py == path_mm_config_py.replace(".py", "_local.py")
    with open(path_mm_config_local_py, "r", encoding="utf-8") as f:
        file_content: str = f.read()
    assert (
        string_replacement_map["MMDETECTION_DIR"] in file_content
    ), "Replacement value 'MMDETECTION_DIR' was not replaced."
    assert not (
        "MMDETECTION_DIR" in file_content
    ), "Replacement value 'MMDETECTION_DIR' was not replaced."


def test_init_mmdetection_config_from_config_path(
    configs: Dict[str, Any],
) -> None:
    """Test if a MMDetection config can be created from its local .py config file."""

    path_mm_config_local_py = configs["path_mm_config_local_py"]

    mm_config: MMConfigMMDetection = init_mmdetection_config(
        config_path=path_mm_config_local_py
    )
    assert isinstance(
        mm_config, MMConfigMMDetection
    ), f"Unexpected type '{type(mm_config)}' returned from init_mmdetection_config."


def test_init_mmdetection_config_from_mm_config(
    configs: Dict[str, Any],
) -> None:
    """Test if a MMDetection config can be created from a config dictionary."""

    mm_config = configs["mm_config"]

    mm_config: MMConfigMMDetection = init_mm_config(mm_config=mm_config)
    assert isinstance(
        mm_config, MMConfigMMDetection
    ), f"Unexpected type '{type(mm_config)}' returned from init_mmdetection_config."


def test_extract_logs_from_file(project_root) -> None:
    logs_path = os.path.join(project_root, "test_data/metrics/yolov3_metrics.json")

    mlcvzoo_logs = extract_logs(logs=parse_logs(file_path=logs_path))

    assert mlcvzoo_logs == {
        1: {
            "train/lr": 0.00010090045022511258,
            "train/data_time": 0.12387418746948242,
            "train/grad_norm": 100.33674621582031,
            "train/loss": 47.78690846761068,
            "train/loss_cls": 4.573872248331706,
            "train/loss_conf": 40.030948638916016,
            "train/loss_xy": 2.762449105580648,
            "train/loss_wh": 0.419638454914093,
            "train/time": 1.4714958667755127,
            "train/iter": 3,
            "train/memory": 1098,
            "train/step": 3,
        },
        2: {
            "train/lr": 0.00010225112556278138,
            "train/data_time": 0.09484549363454182,
            "train/grad_norm": 95.93548583984375,
            "train/loss": 46.88323974609375,
            "train/loss_cls": 4.600101073582967,
            "train/loss_conf": 39.13706906636556,
            "train/loss_xy": 2.781597892443339,
            "train/loss_wh": 0.3644712915023168,
            "train/time": 0.9396946827570597,
            "train/iter": 6,
            "train/memory": 1081,
            "train/step": 6,
        },
        3: {
            "train/lr": 0.00010360180090045023,
            "train/data_time": 0.08271659745110406,
            "train/grad_norm": 95.14725070529514,
            "train/loss": 47.49637137518989,
            "train/loss_cls": 4.586316744486491,
            "train/loss_conf": 39.837713453504776,
            "train/loss_xy": 2.7620436085595026,
            "train/loss_wh": 0.31029656363858116,
            "train/time": 0.7706894609663222,
            "train/iter": 9,
            "train/memory": 1056,
            "train/step": 9,
        },
        4: {
            "train/lr": 0.00010495247623811907,
            "train/data_time": 0.07737594842910767,
            "train/grad_norm": 90.9628168741862,
            "train/loss": 45.87331199645996,
            "train/loss_cls": 4.5562383731206255,
            "train/loss_conf": 38.272345542907715,
            "train/loss_xy": 2.7671249906222024,
            "train/loss_wh": 0.27760224727292854,
            "train/time": 0.6779351433118185,
            "train/iter": 12,
            "train/memory": 997,
            "train/step": 12,
        },
    }


def test_determine_logs_path_no_json(project_root):
    """Test if None is returned if no scalars.json file is present in the given directory."""
    result = determine_logs_path(
        work_dir=os.path.join(
            project_root, "test_data/images"
        )  # no scalars.json present
    )
    assert result is None


def test_determine_logs_path_no_file(project_root, mocker):
    """Test if None is returned if no scalars.json file is present in the given directory but it
    is not a file.
    """
    with mocker.patch("os.path.isfile", return_value=False):
        result = determine_logs_path(
            work_dir=os.path.join(project_root, "test_data/metrics")
        )
    assert result is None
