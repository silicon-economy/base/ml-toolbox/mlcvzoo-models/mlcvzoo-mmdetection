# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests for RTMDet model."""

from pathlib import Path
from typing import Dict, List, Optional

import pytest
from mlcvzoo_base.api.data.bounding_box import BoundingBox
from mlcvzoo_base.api.data.box import Box
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from mlcvzoo_base.api.structs import Runtime

from mlcvzoo_mmdetection.object_detection_model import MMObjectDetectionModel
from mlcvzoo_mmdetection.segmentation_model import MMSegmentationModel
from mlcvzoo_mmdetection.tests.unit_tests.conftest import (
    ExpectedSegmentation,
    verify_bounding_boxes,
    verify_mmdeploy_configs,
    verify_segmentations,
)


@pytest.fixture(name="expected_bounding_boxes")
def expected_bounding_boxes_fixture() -> List[BoundingBox]:
    """Provide a list of expected bounding boxes."""

    return [
        BoundingBox(
            box=Box(xmin=241, ymin=727, xmax=1983, ymax=2194),
            class_identifier=ClassIdentifier(
                class_id=41,
                class_name="cup",
            ),
            score=0.94489586353302,
            difficult=False,
            occluded=False,
            content="",
        ),
        BoundingBox(
            box=Box(xmin=2022, ymin=1722, xmax=3327, ymax=2381),
            class_identifier=ClassIdentifier(
                class_id=43,
                class_name="knife",
            ),
            score=0.5617528557777405,
            difficult=False,
            occluded=False,
            content="",
        ),
    ]


@pytest.fixture(name="expected_segmentations")
def expected_segmentations_fixture() -> List[ExpectedSegmentation]:
    """Provide a list of expected segmentations."""

    return [
        ExpectedSegmentation(
            polygon_count=5221,
            class_identifier=ClassIdentifier(
                class_id=41,
                class_name="cup",
            ),
            score=0.9288867115974426,
            ortho_box=Box(xmin=247.0, ymin=728.0, xmax=1972.0, ymax=2190.0, angle=0.0),
            box=Box(
                xmin=235.0,
                ymin=689.0,
                xmax=1925.0,
                ymax=2173.0,
                angle=4.236394882202148,
            ),
            model_class_identifier=ClassIdentifier(
                class_id=41,
                class_name="cup",
            ),
        ),
        ExpectedSegmentation(
            polygon_count=10566,
            class_identifier=ClassIdentifier(
                class_id=60,
                class_name="diningtable",
            ),
            score=0.5702992677688599,
            ortho_box=Box(xmin=0.0, ymin=1740.0, xmax=3648.0, ymax=2709.0, angle=0.0),
            box=Box(
                xmin=-1.0,
                ymin=1737.5,
                xmax=3647.0,
                ymax=2704.5,
                angle=0.08024613559246063,
            ),
            model_class_identifier=ClassIdentifier(
                class_id=60,
                class_name="diningtable",
            ),
        ),
        ExpectedSegmentation(
            polygon_count=2801,
            class_identifier=ClassIdentifier(
                class_id=43,
                class_name="knife",
            ),
            score=0.47167500853538513,
            ortho_box=Box(
                xmin=2020.0, ymin=1721.0, xmax=3333.0, ymax=2375.0, angle=0.0
            ),
            box=Box(
                xmin=2584.5,
                ymin=1367.0,
                xmax=2797.5,
                ymax=2807.0,
                angle=66.65998840332031,
            ),
            model_class_identifier=ClassIdentifier(
                class_id=43,
                class_name="knife",
            ),
        ),
        ExpectedSegmentation(
            polygon_count=11082,
            class_identifier=ClassIdentifier(
                class_id=60,
                class_name="diningtable",
            ),
            score=0.3119746446609497,
            ortho_box=Box(xmin=0.0, ymin=733.0, xmax=3648.0, ymax=2721.0, angle=0.0),
            box=Box(
                xmin=0.0,
                ymin=733.0,
                xmax=3648.0,
                ymax=2717.0,
                angle=0.06353840976953506,
            ),
            model_class_identifier=ClassIdentifier(
                class_id=60,
                class_name="diningtable",
            ),
        ),
    ]


def test_object_detection_inference(
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
    object_detection_test_image_path: Path,
    expected_bounding_boxes: List[BoundingBox],
) -> None:
    """Test predictions are correct on a test image for the object detection model."""

    config_path = (
        mmdetection_config_path / "rtmdet_coco" / "rtmdet_coco_test_config.yaml"
    )
    model = MMObjectDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
    )

    _, predictions_single = model.predict(
        data_item=str(object_detection_test_image_path)
    )
    predictions_many = model.predict_many(
        data_items=[
            str(object_detection_test_image_path),
            str(object_detection_test_image_path),
        ]
    )

    for predictions in (
        predictions_single,
        predictions_many[0][1],
        predictions_many[1][1],
    ):
        verify_bounding_boxes(
            actual=predictions,
            expected=expected_bounding_boxes,
        )


def test_instance_segmentation_inference(
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
    object_detection_test_image_path: Path,
    expected_segmentations: List[ExpectedSegmentation],
) -> None:
    """Test predictions are correct on a test image for segmentation model."""

    config_path = (
        mmdetection_config_path / "rtmdet_coco" / "rtmdet_ins_coco_test_config.yaml"
    )
    model = MMSegmentationModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
    )

    _, predictions_single = model.predict(
        data_item=str(object_detection_test_image_path)
    )
    predictions_many = model.predict_many(
        data_items=[
            str(object_detection_test_image_path),
            str(object_detection_test_image_path),
        ]
    )

    for predictions in (
        predictions_single,
        predictions_many[0][1],
        predictions_many[1][1],
    ):
        verify_segmentations(
            actual=predictions,
            expected=expected_segmentations,
        )


@pytest.mark.parametrize(
    "runtime,box_coordinate_tolerance,score_tolerance",
    [
        (
            Runtime.ONNXRUNTIME,
            0,
            None,
        ),
        (
            Runtime.ONNXRUNTIME_FLOAT16,
            1,
            6.7e-3,
        ),
        (
            Runtime.TENSORRT,
            7,
            1.1e-1,
        ),
    ],
    ids=["ONNXRUNTIME", "ONNXRUNTIME_FLOAT16", "TENSORRT"],
)
def test_mmdeploy_inference(
    project_root: Path,
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
    object_detection_test_image_path: Path,
    expected_bounding_boxes: List[BoundingBox],
    runtime: str,
    box_coordinate_tolerance: int,
    score_tolerance: Optional[float],
) -> None:
    """Test inference for MMDeploy models."""

    output_base_path = (
        project_root
        / "test_output"
        / "rtmdet_coco_test"
        / "mmdeploy"
        / "static"
        / runtime.lower()
    )

    config_path = (
        mmdetection_config_path
        / "rtmdet_coco"
        / "rtmdet_coco_test_config_mmdeploy_static_config_dict.yaml"
    )
    model = MMObjectDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
        runtime=runtime,
    )

    _, predictions_single = model.predict(str(object_detection_test_image_path))
    predictions_many = model.predict_many(
        [
            str(object_detection_test_image_path),
            str(object_detection_test_image_path),
        ]
    )

    assert Path(
        getattr(
            model.configuration, f"mmdeploy_{runtime.lower()}_config"
        ).checkpoint_path
    ).exists()
    for dump_info_file in ["deploy.json", "detail.json", "pipeline.json"]:
        assert (output_base_path / dump_info_file).exists()

    for predictions in (
        predictions_single,
        predictions_many[0][1],
        predictions_many[1][1],
    ):
        verify_bounding_boxes(
            actual=predictions,
            expected=expected_bounding_boxes,
            box_coordinate_tolerance=box_coordinate_tolerance,
            score_tolerance=score_tolerance,
        )


@pytest.mark.parametrize(
    "runtime",
    [
        Runtime.ONNXRUNTIME,
        Runtime.ONNXRUNTIME_FLOAT16,
        Runtime.TENSORRT,
    ],
)
def test_mmdeploy_configs(
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
    runtime,
) -> None:
    """Test that the MMDeploy Python and the dict configs are equal."""

    py_config_path = (
        mmdetection_config_path
        / "rtmdet_coco"
        / "rtmdet_coco_test_config_mmdeploy_static_config_path.yaml"
    )
    dict_config_path = (
        mmdetection_config_path
        / "rtmdet_coco"
        / "rtmdet_coco_test_config_mmdeploy_static_config_dict.yaml"
    )

    verify_mmdeploy_configs(
        py_config_path=py_config_path,
        dict_config_path=dict_config_path,
        model_type=MMObjectDetectionModel,
        string_replacement_map=string_replacement_map,
        runtime=runtime,
    )
