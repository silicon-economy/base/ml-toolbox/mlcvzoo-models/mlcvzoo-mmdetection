# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests for class MMSegmentationModel."""

from pathlib import Path
from typing import Dict
from unittest import mock

import pytest
from mlcvzoo_base.api.structs import Runtime

from mlcvzoo_mmdetection.segmentation_model import MMSegmentationModel


@pytest.mark.parametrize(
    "runtime",
    [
        Runtime.DEFAULT,
        Runtime.ONNXRUNTIME,
        Runtime.ONNXRUNTIME_FLOAT16,
        Runtime.TENSORRT,
    ],
)
@pytest.mark.parametrize(
    "prediction_function",
    [
        "predict",
        "predict_many",
    ],
)
def test_prediction_no_inferencer(
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
    prediction_function: str,
    runtime: str,
) -> None:
    """Test that an exception is raised, if a model prediction is performed and there is no
    inferencer."""

    config_path = (
        mmdetection_config_path
        / "mask2former"
        / "mask2former_coco_test_config_mmdeploy_static_config_path.yaml"
    )
    model = MMSegmentationModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
        runtime=Runtime.DEFAULT,
    )

    model.runtime = runtime
    model.inferencer = None
    prediction_function = getattr(model, prediction_function)

    with pytest.raises(
        ValueError,
        match="The 'inferencer' attribute is not initialized, make sure to instantiate with "
        "init_for_inference=True",
    ):
        prediction_function(None)


@pytest.mark.parametrize(
    "prediction_function,error_prefix",
    [
        (
            "predict",
            "Prediction",
        ),
        (
            "predict_many",
            "Multi-prediction",
        ),
    ],
    ids=["predict", "predict_many"],
)
def test_predict_runtime_not_supported(
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
    prediction_function: str,
    error_prefix: str,
) -> None:
    """Test that an exception is raised, if a model prediction is performed and the runtime is
    not supported."""

    config_path = (
        mmdetection_config_path
        / "mask2former"
        / "mask2former_coco_test_config_mmdeploy_static_config_path.yaml"
    )
    model = MMSegmentationModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
        runtime=Runtime.DEFAULT,
    )

    model.runtime = "INVALID"
    prediction_function = getattr(model, prediction_function)

    with pytest.raises(
        ValueError, match=f"{error_prefix} is not supported for runtime 'INVALID'."
    ):
        prediction_function(None)


@pytest.mark.parametrize(
    "runtime",
    [
        Runtime.ONNXRUNTIME,
        Runtime.ONNXRUNTIME_FLOAT16,
        Runtime.TENSORRT,
    ],
)
@pytest.mark.parametrize(
    "prediction_function",
    [
        "predict",
        "predict_many",
    ],
)
def test_prediction_fails_if_mmdeploy_not_found(
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
    prediction_function: str,
    runtime: str,
):
    """Test that an exception is raised for a prediction and module mmdeploy is not found."""

    config_path = (
        mmdetection_config_path
        / "mask2former"
        / "mask2former_coco_test_config_mmdeploy_static_config_path.yaml"
    )

    with mock.patch("mlcvzoo_mmdetection.model.MMDeployConverter"):
        with mock.patch("mlcvzoo_mmdetection.model.MMDeployInferencer"):
            model = MMSegmentationModel(
                from_yaml=str(config_path),
                string_replacement_map=string_replacement_map,
                init_for_inference=True,
                runtime=runtime,
            )
            prediction_function = getattr(model, prediction_function)

            with mock.patch(
                "mlcvzoo_mmdetection.segmentation_model.MMDeployInferencer",
                None,
            ):
                with pytest.raises(
                    RuntimeError,
                    match="Extra 'mmdeploy' must be installed to run a model which is deployed "
                    "with MMDeploy.",
                ):
                    prediction_function(None)
