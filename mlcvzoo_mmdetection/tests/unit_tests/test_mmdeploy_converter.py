# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""Tests for class MMDeployConverter."""

from pathlib import Path
from typing import Dict
from unittest.mock import patch

import pytest
from mlcvzoo_base.api.structs import Runtime

from mlcvzoo_mmdetection.mlcvzoo_mmdeploy.converter import MMDeployConverter
from mlcvzoo_mmdetection.object_detection_model import MMObjectDetectionModel
from mlcvzoo_mmdetection.utils import init_mm_config


def test_mmdeploy_converter_backend_type_not_specified(
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
):
    """Test that an exception is raised, if the backend type is not specified in the
    mmdeploy_cfg."""

    config_path = (
        mmdetection_config_path
        / "yolov3_coco"
        / "yolov3_coco_test_config_mmdeploy_static_config_dict.yaml"
    )
    model = MMObjectDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
        runtime=Runtime.DEFAULT,
    )

    mmdeploy_converter = MMDeployConverter(
        model_config=model.cfg,
        checkpoint_path=None,
        mmdeploy_config=model.configuration.mmdeploy_onnxruntime_config,
    )

    mocked_mmdeploy_cfg = init_mm_config(
        mm_config=model.configuration.mmdeploy_onnxruntime_config
    )

    # Missing backend_config.type
    # pylint: disable=protected-access
    del mocked_mmdeploy_cfg._cfg_dict["backend_config"]["type"]

    with patch.object(
        mmdeploy_converter,
        "mmdeploy_cfg",
        mocked_mmdeploy_cfg,
    ):
        with pytest.raises(
            KeyError,
            match="Invalid MMDeploy configuration. Key 'backend_config.type' is not specified.",
        ):
            mmdeploy_converter.run()

    # Missing backend_config
    # pylint: disable=protected-access
    del mocked_mmdeploy_cfg._cfg_dict["backend_config"]

    with patch.object(
        mmdeploy_converter,
        "mmdeploy_cfg",
        mocked_mmdeploy_cfg,
    ):
        with pytest.raises(
            KeyError,
            match="Invalid MMDeploy configuration. Key 'backend_config.type' is not specified.",
        ):
            mmdeploy_converter.run()


def test_mmdeploy_converter_backend_type_not_supported(
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
):
    """Test that an exception is raised, if the backend type in the mmdeploy_cfg is not
    supported."""

    config_path = (
        mmdetection_config_path
        / "yolov3_coco"
        / "yolov3_coco_test_config_mmdeploy_static_config_dict.yaml"
    )

    model = MMObjectDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
        runtime=Runtime.DEFAULT,
    )

    mmdeploy_converter = MMDeployConverter(
        model_config=model.cfg,
        checkpoint_path=None,
        mmdeploy_config=model.configuration.mmdeploy_onnxruntime_config,
    )

    mocked_mmdeploy_cfg = init_mm_config(
        mm_config=model.configuration.mmdeploy_onnxruntime_config
    )
    mocked_mmdeploy_cfg["backend_config"]["type"] = "foobar"

    with patch.object(
        mmdeploy_converter,
        "mmdeploy_cfg",
        mocked_mmdeploy_cfg,
    ):
        with pytest.raises(
            NotImplementedError,
            match="Invalid MMDeploy configuration. The backend type 'foobar' is not supported.",
        ):
            mmdeploy_converter.run()


def test_mmdeploy_converter_onnxruntime_cuda_not_supported(
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
):
    """Test that an exception is raised, if the backend type in the mmdeploy_cfg is onnxruntime
    and the device_string in the mmdeploy_runtime_config is cuda."""

    config_path = (
        mmdetection_config_path
        / "yolov3_coco"
        / "yolov3_coco_test_config_mmdeploy_static_config_dict.yaml"
    )
    model = MMObjectDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
        runtime=Runtime.DEFAULT,
    )

    mmdeploy_converter = MMDeployConverter(
        model_config=model.cfg,
        checkpoint_path=None,
        mmdeploy_config=model.configuration.mmdeploy_onnxruntime_config,
    )

    with patch.object(
        mmdeploy_converter.mmdeploy_config,
        "device_string",
        "cuda",
    ):
        with pytest.raises(
            ValueError, match="Backend onnxruntime is only supported on CPU."
        ):
            mmdeploy_converter.run()


def test_mmdeploy_converter_tensorrt_cpu_not_supported(
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
):
    """Test that an exception is raised, if the backend type in the mmdeploy_cfg is tensorrt
    and the device_string in the mmdeploy_runtime_config is cpu."""

    config_path = (
        mmdetection_config_path
        / "yolov3_coco"
        / "yolov3_coco_test_config_mmdeploy_static_config_dict.yaml"
    )
    model = MMObjectDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
        runtime=Runtime.DEFAULT,
    )

    mmdeploy_converter = MMDeployConverter(
        model_config=model.cfg,
        checkpoint_path=None,
        mmdeploy_config=model.configuration.mmdeploy_tensorrt_config,
    )

    with patch.object(
        mmdeploy_converter.mmdeploy_config,
        "device_string",
        "cpu",
    ):
        with pytest.raises(
            ValueError, match="Backend tensorrt is only supported on GPU."
        ):
            mmdeploy_converter.run()


@patch("mlcvzoo_mmdetection.mlcvzoo_mmdeploy.converter.export2SDK")
@patch("mlcvzoo_mmdetection.mlcvzoo_mmdeploy.converter.get_ir_config")
@patch("mlcvzoo_mmdetection.mlcvzoo_mmdeploy.converter.torch2onnx")
@patch("mlcvzoo_mmdetection.mlcvzoo_mmdeploy.converter.get_backend")
@patch("mlcvzoo_mmdetection.mlcvzoo_mmdeploy.converter.to_backend")
@patch("mlcvzoo_mmdetection.mlcvzoo_mmdeploy.converter.shutil.move")
def test_mmdeploy_converter_success_path(
    move_mock,  # pylint: disable=unused-argument
    to_backend_mock,  # pylint: disable=unused-argument
    get_backend_mock,  # pylint: disable=unused-argument
    torch2onnx_mock,  # pylint: disable=unused-argument
    get_ir_config_mock,  # pylint: disable=unused-argument
    export_2_sdk_mock,  # pylint: disable=unused-argument
    mmdetection_config_path: Path,
    string_replacement_map: Dict[str, str],
):
    """Test that no exceptions are raised, if the MMDeploy config is valid."""

    config_path = (
        mmdetection_config_path
        / "yolov3_coco"
        / "yolov3_coco_test_config_mmdeploy_static_config_dict.yaml"
    )
    model = MMObjectDetectionModel(
        from_yaml=str(config_path),
        string_replacement_map=string_replacement_map,
        init_for_inference=True,
        runtime=Runtime.DEFAULT,
    )

    mmdeploy_converter = MMDeployConverter(
        model_config=model.cfg,
        checkpoint_path=None,
        mmdeploy_config=model.configuration.mmdeploy_onnxruntime_config,
    )

    mmdeploy_converter.run()
