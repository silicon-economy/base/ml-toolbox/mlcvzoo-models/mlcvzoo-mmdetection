# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Module for handling utility methods that are used across the
mlcvzoo_mmdetection package.
"""

import json
import logging
import os
from pathlib import Path
from typing import Any, Dict, List, Optional, Tuple

from mlcvzoo_base.api.data.types import MetricInfo
from mmengine.config import Config

from mlcvzoo_mmdetection.configuration import MMConfig

logger = logging.getLogger(__name__)


def init_mmdetection_config(
    config_path: str,
    cfg_options: Optional[Dict[str, Any]] = None,
) -> Config:
    """
    Note: This function is deprecated and will be removed in future versions. Please use
    'mlcvzoo_mmdetection.utils.init_mm_config' instead.

    Initialize an mmengine.config.Config object from a given mmdetection .py config file.

    Args:
        config_path: Filepath of the mmdetection .py config file.
        cfg_options: Overwrite / additional options to load into the mmengine Config object.

    Returns:
        The mmengine Config object.
    """
    logger.warning(
        "DeprecationWarning: 'mlcvzoo_mmdetection.utils.init_mmdetection_config' "
        "is deprecated and will be removed in future versions. Please use "
        "'mlcvzoo_mmdetection.utils.init_mm_config' instead."
    )

    return init_mm_config(
        mm_config=MMConfig(config_path=config_path, cfg_options=cfg_options)
    )


def init_mm_config(mm_config: MMConfig) -> Config:
    """
    Initialize an mmengine.config.Config object from a given
    mmlcvzoo_mmdetection.configuration.MMConfig object. Currently,
    the object can be initialized from its mmdetection .py config
    file as well as from a python dictionary. Please note that
    initialization from .py config files will be removed in future
    releases (cf. documentation for 'mlcvzoo_mmdetection.utils.init_mmdetection_config').

    Args:
        mm_config: The MMConfig object.

    Returns:
        The mmengine Config object.
    """
    if mm_config.config_path is not None:
        # Build config from .py configuration file
        logger.info(
            "Load mmdetection config from config-path: %s", mm_config.config_path
        )
        cfg = Config.fromfile(mm_config.config_path)
    elif mm_config.config_dict is not None:
        logger.info(
            "Load mmdetection config from config-dict: %s", mm_config.config_dict
        )
        cfg = Config(cfg_dict=mm_config.config_dict)
    else:
        raise ValueError(
            "Can not build mmengine config, either config-path or config-dict have to be"
            "provided in mm_config"
        )

    if mm_config.cfg_options:
        cfg.merge_from_dict(mm_config.cfg_options)

    return cfg


def modify_config(config_path: str, string_replacement_map: Dict[str, str]) -> str:
    """
    Note: This function is deprecated and will be removed in future versions.

    Load a mmdetection .py config file via its filepath and replace all matching keys
    (= placeholder / environment variables) in the file with their corresponding values
    in the string_replacement_map. The replaced file content is then stored in a *_local.py
    file in the same directory.

    Args:
        config_path: Path to the .py config file.
        string_replacement_map: Dictionary with string replacement values, e.g.
                                {"PROJECT_ROOT_DIR": "your/local/project/dir"}

    Returns:
        The filepath of the modified *_local.py file.
    """
    with open(file=config_path, mode="r", encoding="'utf-8") as config_file:
        config_file_content = config_file.readlines()

    new_config_file_content = list()
    for config_content in config_file_content:
        new_config_content = config_content

        for replacement_key, replacement_value in string_replacement_map.items():
            if replacement_key in config_content:
                new_config_content = new_config_content.replace(
                    replacement_key, replacement_value
                )

                logger.info(
                    "Replace '%s' in config-line '%s' with '%s'",
                    replacement_key,
                    new_config_content,
                    replacement_value,
                )

        new_config_file_content.append(new_config_content)

    new_config_path = config_path.replace(".py", "_local.py")
    with open(file=new_config_path, mode="w+", encoding="'utf-8") as new_config_file:
        new_config_file.writelines(new_config_file_content)

    return new_config_path


def determine_logs_path(work_dir: str) -> Optional[str]:
    """
    Since there can be logs of different training runs in the same
    working directory, determine the log file path based on the last
    in lexically sorted order, since the logs can be sorted according
    to their timestamp.

    Args:
        work_dir: The working directory where the logs have been produced

    Returns:
        The latest metric file found in a working directory of an mmengine training.
    """

    logs_json_paths = [str(p) for p in Path(work_dir).rglob("scalars.json")]
    if not logs_json_paths:
        return None

    logs_json_paths.sort()
    logs_json_path = logs_json_paths[-1]
    if not os.path.isfile(logs_json_path):
        return None

    return logs_json_path


def parse_logs(file_path: str) -> List[Dict[str, Any]]:
    """
    Parse all log entries from a given file that has been
    produced by the mmengine logger. In the file each line
    is in json format, but the full file is not fully json
    compliant. Therefore, parse a list of dictionaries,
    which represent the log file content.

    Args:
        file_path: The file from which to parse the content

    Returns:
        The file content as a list of dictionaries
    """
    logs = []
    with open(file_path, "r") as log_file:
        lines = log_file.readlines()

    for line in lines:
        logs.append(json.loads(line))

    return logs


def extract_logs(
    logs: List[Dict[str, Any]],
) -> Dict[int, Dict[str, float]]:
    """Extract logs that have been generated from a openmmlab training and
    stores them in a dictionary that is expected by the MLCVZoo interface MetricProvider

    Output format
    {
        epoch: {
                train_metric_name: metric_value,
                val_metric_name: metric_value
        }
    }

    extracted_logs:
    - 1st Key: epoch
    - 2nd Key: metric_name
    - value: Metric value

    Args:
        logs: The logs to extract.

    Returns:
        The extracted logs.
    """
    extracted_logs: Dict[int, Dict[str, float]] = {}

    for log_entry in logs:

        try:
            epoch = log_entry.pop("epoch")
            prefix = "train"
        except KeyError:
            try:
                # The logger in mmengine uses "step" instead of "epoch" for the validation logs
                epoch = log_entry.pop("step")
                prefix = "val"
            except KeyError:
                continue

        if epoch not in extracted_logs:
            extracted_logs[epoch] = {}
        for log_name, log_value in log_entry.items():
            # NOTE: There can be multiple entries for the same epoch, since the logging
            #       interval is defined via an interval on a per-step basis. Therefore,
            #       we override an existing epoch entry, and by that taking the last entry
            #       as representative for the epoch.
            extracted_logs[epoch][f"{prefix}/{log_name}"] = log_value

    return extracted_logs


def extract_best_metric(
    work_dir: str,
    filename_template: str,
    logs: Dict[int, Dict[str, float]],
    best_metric_name: str,
) -> Tuple[Optional[MetricInfo], Dict[str, MetricInfo]]:
    """Extract the best metric from the logs and return the corresponding MetricInfo object.

    The best metric is the metric with the highest score.

    Args:
        work_dir: The directory where the checkpoints are stored.
        filename_template: The template for the checkpoint filename.
        logs: The logs from the training run.
        best_metric_name: The name of the metric to consider.
    """

    best_metric_info: Optional[MetricInfo] = None
    metrics: Dict[str, MetricInfo] = {}

    for epoch, log_entry in logs.items():
        for log_entry_name, log_value in log_entry.items():
            if log_entry_name == best_metric_name:
                checkpoint_path = str(
                    os.path.join(
                        work_dir,
                        filename_template.format(epoch),
                    )
                )
                metric_info = MetricInfo(
                    path=checkpoint_path,
                    epoch=epoch,
                    score=log_value,
                    name=log_entry_name,
                )
                metrics[checkpoint_path] = metric_info

                if (
                    best_metric_info is None
                    or best_metric_info.score < metric_info.score
                ):
                    best_metric_info = metric_info

    return best_metric_info, metrics
