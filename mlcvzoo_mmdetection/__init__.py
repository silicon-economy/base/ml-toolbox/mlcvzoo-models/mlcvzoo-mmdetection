# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

import os

__version__ = "6.8.0" + os.environ.get("PACKAGE_VERSION_EXTENSION", "")
__license__ = "OLFL-1.3"
